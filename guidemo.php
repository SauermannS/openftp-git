<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . "include" . DIRECTORY_SEPARATOR . "includes.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>GUI Demo</title>
	<script type="text/javascript" src="include/lib/jquery.js"></script>
    <script type="text/javascript" src="include/lib/module.js"></script>
	<script type="text/javascript">

		$(document).ready(function(){
						var m = new Module("Demomodule");
						m.loadModule(Array());
					});
					
		function openModule(moduleName)
		{
			var m = new Module(moduleName);
			m.loadModule(Array());	
		}
	</script>
</head>
<body>
	<div id="header">
		<h2>Etwas Men&uuml;artiges =)</h2>
        <br/>
        <a href="javascript:void(0)" onClick="openModule('Demomodule');">Demomodule</a>
        <br/>
        <a href="javascript:void(0)" onClick="openModule('Demomodule2');">Demomodule2</a>
        <br/>
        <a href="javascript:void(0)" onClick="openModule('Demomodule3');">Demomodule3 (Nicht vorhanden)</a>
        <br/><br/>
        <h2>Der Inhalt</h2>
	</div>
	<div id="content">

	</div>
    <div id="footer">
		<br/><br/>
        <h2>Der Footer</h2>
        &copy; 2012 und so...
	</div>
</body>
</html>