<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . "include" . DIRECTORY_SEPARATOR . "includes.php");
use OpenFTP\Classes as Clss;
use OpenFTP\Classes\Exceptions as Ex;
use OpenFTP\Packets\ScreenOut;

//Modul als Parameter übergeben
if(isset($_POST['module']) && !empty($_POST['module'])) {

	//Modulnamen
	$module = $_POST['module']; 
	  
	//Vorhandensein des Moduls abfragen
	try
	{
		$moduleObj = new Clss\Module($module);
	}
	catch (Ex\ModuleNotFoundException $ex)
	{
		die(ScreenOut::getMessageInLanguage("EXCEPTION_MODULE_NOTFOUND"));
	}
    
	//Prüfen, ob Modul aktiv ist
	$moduleID = Clss\Module::getModuleID($module);
	if (!$moduleObj->getIsActive())
   		die(ScreenOut::getMessageInLanguage("EXCEPTION_MODULE_DEACTIVATED"));
    
    //Zugriff für Benutzer auf Modul überprüfen
    if (Clss\Permissions::getAllowModule($moduleID, Clss\User::getSessionID()))
    {
   		$moduleNamespaceWithName = "OpenFTP\Module\\" . $module;
    	$m = new $moduleNamespaceWithName;
    }
    else
    {
    	die(ScreenOut::getMessageInLanguage("EXCEPTION_MODULE_PERMISSIONDENIED"));
    }

	//Modul anzeigen
    $m->show();
    
}
?>