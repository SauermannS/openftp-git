/*!
 * Modulklasse zum Laden von Modulen per AJAX
 * 
 * Copyright 2012, Sascha Sauermann
 * 
 * Benoetigt jQuery
 * http://jquery.com/
 */
function Module(moduleName)
{
	this.moduleName = moduleName;
	this.contentDiv = 'content';
	this.pathToPHP = './';
	
	var self = this;

	this.loadModule = function (params)
	{
		$.post(self.pathToPHP + "show.php",
			{module: self.moduleName, params: params},
			function(data){self.setContent(data)});
	};

	this.processModule = function (params)
	{
		$.post(self.pathToPHP + "process.php",
			{module: self.moduleName, params: params},
			function(data){self.setContent(data)});
	};

	this.setContent = function (content)
	{
		$('#' + self.contentDiv).empty();
		$('#' + self.contentDiv).html(content);
	};
}