<?php
namespace OpenFTP\Packets;

use OpenFTP\Classes\Settings;

use OpenFTP\Classes;
use OpenFTP\Classes\Exceptions;

/**
 * Klasse für die Aufbereitung von Informationen und Nachrichten zur Ausgabe an den Benutzer.
 * @author Sascha Sauermann
 * @since 0.1
 */
class ScreenOut
{
	/**
	 * -NachrichtInSprache-
	 * Gibt den in der Datenbank gespeicherten Wert für ein Sprachelement zurück.
	 * Reihenfolge: Session -> Default -> EN
	 * Wenn Sprache in Session zwar gesetzt ist, jedoch nicht verfügbar ist, wird der Session Wert gelöscht.
	 * Ebenso mit dem Default Wert.
	 * @param String $element Sprachelement
	 * @return String Wert für das Sprachelement
	 * @since 0.1
	 */
	static function getMessageInLanguage($element)
	{
		//Sprache aus Session auslesen
		$userLanguage = $_SESSION['LANGUAGE'];
		//Standard Nachricht = ""
		$message = "";
		
		//Wenn userLanguage gesetzt ist
		if (isset($userLanguage) && $userLanguage!= "")
		{
			try 
			{
				//Sprache angeben
				$lang = new Classes\Language($userLanguage);
				//Nachricht abfragen in Sprache
				$message = $lang->getValue($element);
				//Nachricht returnen
				return $message;

			}
			//Angegebene Sprache nicht vorhanden
			catch (Exceptions\LanguageNotFoundException $ex)
			{
				//Spracheinstellung in Session leeren.
				unset($_SESSION['LANGUAGE']);
			}
			//Element in dieser Sprache nicht vorhanden
			catch (Exceptions\LanguageElementNotFoundException $ex)	{}
			
		}
		if (Classes\Settings::getSetting("DEFAULT_LANGUAGE",TRUE)!=NULL)
		{
			try 
			{
				//Sprache nicht angeben oder gesetzte Sprache nicht verfügbar
				$lang = new Classes\Language();
				//Nachricht abfragen in Defaultsprache/Englisch
				$message = $lang->getValue($element);
				//Nachricht returnen
				return $message;
			}
			//Defaultsprache nicht vorhanden
			catch (Exceptions\LanguageNotFoundException $ex)
			{
				Classes\Settings::delSetting("DEFAULT_LANGUAGE");
			}
			//Element in Standardsprache nicht vorhanden
			catch (Exceptions\LanguageElementNotFoundException $ex)
			{
				//Element in [] mit Sprache zurückgeben
				return "[".$element." (".$ex->getLanguage().")]";
			}
		}
		
		try 
		{
			//Sprache nicht angeben oder gesetzte Sprache nicht verfügbar
			$lang = new Classes\Language();
			//Nachricht abfragen in Defaultsprache/Englisch
			$message = $lang->getValue($element);
			//Nachricht returnen
			return $message;
		}
		//Englisch nicht vorhanden -> Fehler blubbern lassen
		//catch (Exceptions\LanguageNotFoundException $ex)

		//Element in Englisch nicht vorhanden
		catch (Exceptions\LanguageElementNotFoundException $ex)
		{
			//Element in [] mit Sprache zurückgeben
			return "[".$element." (".$ex->getLanguage().")]";
		}
	}
	
}


?>