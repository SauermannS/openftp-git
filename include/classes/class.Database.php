<?php
namespace OpenFTP\Classes;
/**
 * Datenbankklasse zur Verwendung einer MySQL Datenbank.
 *
 * @author Sascha Sauermann
 * @since 0.1
 * @package OpenFTP\Classes
 *             
 * Abstraktionsschicht für die Datenbank
 *            
 * Verbindet zur Datenbank und kapselt alle Anfragen an die
 * Datenbank. Nutzbar mit diversen Datenbanken (MySQL, SQLite,
 * Oracle, etc.)
 *            
 */
class Database {
	
	/**
	 * Datenbankobjekt
	 * @var \PDO
	 * @since 0.1
	 */
	public $PDO = null;
	/**
	 * Aktuelles preparedStatement
	 * @var \PDOStatement
	 * @since 0.1
	 */
	public $preparedStatement = null;
	/**
	 * -Konstruktor-
	 * Verbindet zur Datenbank und gibt ggf. eine Fehlermeldung zurück.
	 * @param $dsn String Verbindungszeichenfolge für die Verbindung	
	 * @param $user String Benutzername für die Anmeldung
	 * @param $password String Passwort für die Anmeldung
	 * @example <code>Database('mysql:host='.DB_SERVER.';port='.DB_PORT.';dbname='.DB_DB, DB_USER, DB_PASSWORD)</code>
	 * @since 0.1
	 * @throws Exceptions\DBConnectionException
	 */
	public function __construct($dsn, $user, $password) {
		try {
			// Neues PDO-Objekt
			$this->PDO = new \PDO ( $dsn, $user, $password );
			// Fehlermeldungen sollen "geworfen" werden
			$this->PDO->setAttribute ( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
		} catch (\PDOException $e ) {
			throw new Exceptions\DBConnectionException($dsn,$e);
		}
	
	}
	
	/**
	 * -SQL-Abfrage-
	 * Führt eine SQL-Anfrage durch.
	 * @param $sql String SQL Abfrage
	 * @return Array Ergebnismenge der Abfrage
	 * @since 0.1
	 * @throws Exceptions\DBQueryException
	 */
	public function query($sql) {
		try 
		{
			// PDO-Anfrage durchführen
			$pdoStmt = $this->PDO->query ($sql);
			// Liegt eine leere Ergebnismenge vor?
			($pdoStmt->rowCount () == 0) ? $return = Array ():
			// Array mit Daten
			$return = $pdoStmt->fetchAll ();
			// Statement schließen
			$pdoStmt->closeCursor ();
			return $return;
		}	
		catch ( \PDOException $e ) {		
			throw new Exceptions\DBQueryException($sql, NULL, $e);	
		}
	
	}
	
	/**
	 * -Prepared Statement anlegen-
	 * Legt ein "prepared Statement" an.
	 * @example <code>prepareStatement('SELECT * FROM :platzhalter');</code>
	 * @param $statement String Statement mit Platzhalter-Parametern.
	 * @since 0.1
	 * @throws Exceptions\DBStatementPreparationException
	 */
	public function prepareStatement($statement) {
		// Prepared Statement vorbereiten
		try 
		{
			$this->preparedStatement = $this->PDO->prepare ( $statement );
		}
		catch (\PDOException $e)
		{
			throw new Exceptions\DBStatementPreparationException($statement,$e);
		}
	}
	
	/**
	 * -Execute Prepared Statement-
	 * Führt ein zuvor angelegtes preparedStatement aus
	 * 
	 * <code>
	 * execute(Array('platzhalter1'=>'wert1','platzhalter3'=>'wert3'));
	 * execute(Array('wert','wert2'));
	 * </code>
	 * 
	 * @param $params Array [optional=Array()]	Die Parameter für das prepared Statement.
	 * @return Array Array der Anfrage oder ein leeres Array
	 * @since 0.1
	 * @throws Exceptions\DBNoStatementException
	 * @throws Exceptions\DBQueryException
	 */
	
	public function execute($params = Array()) {
		// Wenn noch kein Statement angelegt ist, wird hier abgebrochen.
		if ($this->preparedStatement == NULL)
			throw new Exceptions\DBNoStatementException();
		
		try 
		{
			// PDO-Anfrage durchführen
			$this->preparedStatement->execute ($params);
			// Wenn keine Daten zurück kamen
			if ($this->preparedStatement->columnCount () == 0)
				return Array();
			else 
				// Andernfalls die Daten als Array zurückgeben
				return $this->preparedStatement->fetchAll ();
		
		} 
		catch (\PDOException $e ) 
		{
			throw new Exceptions\DBExecuteStatementException($params, $e);
		}
	}
	
	/**
	 * -Beeinflusste Datensätze-
	 * Gibt die Anzahl der beeinflussten Datensätze zurück-
	 * @return integer Anzahl beinflusster Datensätze
	 * @throws Exceptions\DBNoStatementException
	 * @throws Exceptions\DBPDOFunctionException
	 * @since 0.1
	 */
	public function affectedRows() {
		// Wenn noch kein Statement angelegt ist, wird hier abgebrochen.
		if ($this->preparedStatement == NULL)
			throw new Exceptions\DBNoStatementException();
		try 
		{
			return $this->preparedStatement->rowCount();	
		}
		catch (\PDOException $e)
		{
			throw new Exceptions\DBPDOFunctionException($e);	
		}
	}
	
	/**
	 * -Zuletzt eingefügte ID abfragen-
	 * Gibt die ID oder den Wert einer bestimmten Spalte aus, der mit der
	 * letzten SQL Abfrage eingefügt wurde.
	 * 
	 * @param String $column [optional] Name der Spalte, aus der der Wert des letzten Eintrags ausgelesen werden soll.
	 * @return String Zuletzt eingefügte ID, oder zuletzt eingefügtes Feld in der ausgewählten Spalte.
	 * @throws Exceptions\DBPDOFunctionException
	 * @since 0.1
	 */
	public function lastInsertID($column = NULL) {
		try {
			return $this->PDO->lastInsertId ( $column );
		} catch (\PDOException $e) {
			throw new Exceptions\DBPDOFunctionException($e);
		}

	}

}
?> 