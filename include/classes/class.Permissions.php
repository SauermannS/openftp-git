<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Rechteverwaltung
 * 
 * @author Sascha Sauermann
 * @since 0.1
 */
class Permissions {
	
	/**
	 * -Zugriff auf Modul überprüfen-
	 * Überprüft den Zugriff auf ein Modul:
	 * Reihenfolge:
	 * +Userpermission
	 * +Gruppenpermission
	 * +Defaultpermission
	 * @param integer $moduleID
	 * @param integer $userID
	 * @return boolean Zugriffserlaubnis
	 * @since 0.1
	 */
	public static function getAllowModule($moduleID,$userID) {
		//Standardpermission für Benutzer abfragen
		$permission = Permissions::getAllowDefaultPermissionsUser($userID, $moduleID);
		//Zugriff erlaubt
		if ($permission===TRUE){
			return TRUE;
		}
		//Zugriff verboten
		elseif ($permission===FALSE){
			return FALSE;
		}
		//Nicht definiert
		else
		{
			//Aufräumen
			$permission = NULL;
			
			//Benutzer mit userID verwenden
			$user = new User(User::getName($userID));
						
			//Gruppen des Benutzers abfragen
			$groups = $user->getGroups();
			
			//Gibt an, ob der Zugriff einer Gruppe des Benutzers bereits untersagt war
			$forbidden = FALSE;
			//Zugriff für jede Gruppe des Benutzers überprüfen
			foreach ($groups as $group)
			{
				$permission = Permissions::getAllowDefaultPermissionsGroup($group, $moduleID);
				//Zugriff erlaubt
				if ($permission === TRUE) {
					return TRUE;
				}
				//Zugriff für diese Gruppe verboten
				//->Merken und andere Gruppen überprüfen, ob nicht doch irgendwo erlaubt
				elseif ($permission === FALSE) {
					$forbidden = TRUE;
				}
			}
			//Wenn Zugriff für Gruppe in irgendeiner Gruppe verboten war, Zugriff verweigern
			if ($forbidden===TRUE) {
				return FALSE;
			}
			//In keiner Gruppe Zugriff definiert
			else
			{
				//Aufräumen
				$user = NULL;
				$permission = NULL;
				$forbidden = NULL;
				$groups = NULL;
				
				//Angebenes Modul verwenden
				$module = new Module(Module::getName($moduleID));
				$permission = $module->getDefaultPermission();
				//Zugriff gewähren
				if ($permission===TRUE) {
					return TRUE;
				}
				//Wenn nirgends definiert oder beim Modul verboten, Zugriff verbieten	
				else return FALSE;
			}
		}	
	}
	
	/**
	 * -Zugriff auf Datei in Modul überprüfen-
	 * Überprüft den Zugriff auf ein Modul mit einer bestimmten Datei:
	 * Reihenfolge:
	 * +Userfilepermission
	 * +Groupfilepermission
	 * +Userpermission
	 * +Gruppenpermission
	 * +Defaultpermission
	 * @param integer $moduleID
	 * @param integer $userID
	 * @param integer $fileID
	 * @return boolean Zugriffserlaubnis
	 * @since 0.1
	 */
	public static function getAllowFile($moduleID,$userID,$fileID) {
		//Permission für Benutzer bei Datei abfragen
		$permission = Permissions::getAllowFileUser($fileID, $userID, $moduleID);
		//Zugriff erlaubt
		if ($permission===TRUE){
			return TRUE;
		}
		//Zugriff verboten
		elseif ($permission===FALSE){
			return FALSE;
		}
		//Nicht definiert
		else
		{
			//Aufräumen
			$permission = NULL;
			
			//Benutzer mit userID verwenden
			$user = new User(User::getName($userID));
						
			//Gruppen des Benutzers abfragen
			$groups = $user->getGroups();
			
			//Gibt an, ob der Zugriff einer Gruppe des Benutzers bereits untersagt war
			$forbidden = FALSE;
			//Zugriff für jede Gruppe des Benutzers überprüfen
			foreach ($groups as $group)
			{
				$permission = Permissions::getAllowFileGroup($fileID, $group, $moduleID);
				//Zugriff erlaubt
				if ($permission === TRUE) {
					return TRUE;
				}
				//Zugriff für diese Gruppe verboten
				//->Merken und andere Gruppen überprüfen, ob nicht doch irgendwo erlaubt
				elseif ($permission === FALSE) {
					$forbidden = TRUE;
				}
			}
			//Wenn Zugriff für Gruppe in irgendeiner Gruppe verboten war, Zugriff verweigern
			if ($forbidden===TRUE) {
				return FALSE;
			}
			//In keiner Gruppe Zugriff definiert
			else
			{
				//Aufräumen
				$user = NULL;
				$permission = NULL;
				$forbidden = NULL;
				$groups = NULL;
				
				//Zugriff auf Modul allgemein überprüfen
				$permission = Permissions::getAllowModule($moduleID, $userID);
								
				//Zugriff gewähren
				if ($permission===TRUE) {
					return TRUE;
				}
				//Wenn nirgends definiert oder beim Modul verboten, Zugriff verbieten	
				else return FALSE;
			}
			
			
		}
		
	}
	
	/**
	 * -Zugriff auf Ordner in Modul überprüfen-
	 * Überprüft den Zugriff auf ein Modul mit einem bestimmten Ordner:
	 * Reihenfolge:
	 * +Userfolderpermission
	 * +Groupfolderpermission
	 * +Userpermission
	 * +Gruppenpermission
	 * +Defaultpermission
	 * @param integer $moduleID
	 * @param integer $userID
	 * @param integer $folderID
	 * @return boolean Zugriffserlaubnis
	 * @since 0.1
	 */	
	public static function getAllowFolder($moduleID,$userID,$folderID) {
		//Permission für Benutzer bei Datei abfragen
		$permission = Permissions::getAllowFolderUser($folderID, $userID, $moduleID);
		//Zugriff erlaubt
		if ($permission===TRUE){
			return TRUE;
		}
		//Zugriff verboten
		elseif ($permission===FALSE){
			return FALSE;
		}
		//Nicht definiert
		else
		{
			//Aufräumen
			$permission = NULL;
			
			//Benutzer mit userID verwenden
			$user = new User(User::getName($userID));
					
			//Gruppen des Benutzers abfragen
			$groups = $user->getGroups();
			
			//Gibt an, ob der Zugriff einer Gruppe des Benutzers bereits untersagt war
			$forbidden = FALSE;
			//Zugriff für jede Gruppe des Benutzers überprüfen
			foreach ($groups as $group)
			{
				$permission = Permissions::getAllowFolderGroup($folderID, $group, $moduleID);
				//Zugriff erlaubt
				if ($permission === TRUE) {
					return TRUE;
				}
				//Zugriff für diese Gruppe verboten
				//->Merken und andere Gruppen überprüfen, ob nicht doch irgendwo erlaubt
				elseif ($permission === FALSE) {
					$forbidden = TRUE;
				}
			}
			//Wenn Zugriff für Gruppe in irgendeiner Gruppe verboten war, Zugriff verweigern
			if ($forbidden===TRUE) {
				return FALSE;
			}
			//In keiner Gruppe Zugriff definiert
			else
			{
				//Aufräumen
				$user = NULL;
				$permission = NULL;
				$forbidden = NULL;
				$groups = NULL;
				
				//Zugriff auf Modul allgemein überprüfen
				$permission = Permissions::getAllowModule($moduleID, $userID);
								
				//Zugriff gewähren
				if ($permission===TRUE) {
					return TRUE;
				}
				//Wenn nirgends definiert oder beim Modul verboten, Zugriff verbieten	
				else 
					return FALSE;
			}
			
			
		}
		
	}
	
	/**
	 * -Gruppenzugriff auf Ordner überprüfen-
	 * @param integer $folder OrdnerID
	 * @param integer $group GruppenID
	 * @param integer $module ModulID
	 * @return boolean Zugriffserlaubnis/-verbot oder NULL wenn keine Permission gesetzt ist.
	 * @since 0.1
	 */
	private static function getAllowFolderGroup($folder,$group,$module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `allow` FROM `folderPermissionsGroup` WHERE `folder`=:folder AND `group`=:group AND `module`=:module");
		$data = $DB->execute(Array("folder"=>$folder,"group"=>$group,"module"=>$module));
		//Zugriff erlaubt, verboten, nicht festgelegt
		if (count($data)>0)
			return (boolean) $data[0]['allow'];
		else
			return NULL;
	}
	
	/**
	 * -Benutzerzugriff auf Ordner überprüfen-
	 * @param integer $folder OrdnerID
	 * @param integer $user BenutzerID
	 * @param integer $module ModulID
	 * @return boolean Zugriffserlaubnis/-verbot oder NULL wenn keine Permission gesetzt ist.
	 * @since 0.1
	 */
	private static function getAllowFolderUser($folder,$user,$module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `allow` FROM `folderPermissionsUser` WHERE `folder`=:folder AND `user`=:user AND `module`=:module");
		$data = $DB->execute(Array("folder"=>$folder,"user"=>$user,"module"=>$module));
		//Zugriff erlaubt, verboten, nicht festgelegt
		if (count($data)>0)
			return (boolean) $data[0]['allow'];
		else
			return NULL;
	}

	/**
	 * -Gruppenzugriff auf Datei überprüfen-
	 * @param integer $file DateiID
	 * @param integer $group GruppenID
	 * @param integer $module ModulID
	 * @return boolean Zugriffserlaubnis/-verbot oder NULL wenn keine Permission gesetzt ist.
	 * @since 0.1
	 */
	private static function getAllowFileGroup($file,$group,$module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `allow` FROM `filePermissionsGroup` WHERE `file`=:file AND `group`=:group AND `module`=:module");
		$data = $DB->execute(Array("file"=>$file,"group"=>$group,"module"=>$module));
		//Zugriff erlaubt, verboten, nicht festgelegt
		if (count($data)>0)
			return (boolean) $data[0]['allow'];
		else
			return NULL;
	}
	
	/**
	 * -Benutzerzugriff auf Datei überprüfen-
	 * @param integer $file DateiID
	 * @param integer $user BenutzerID
	 * @param integer $module ModulID
	 * @return boolean Zugriffserlaubnis/-verbot oder NULL wenn keine Permission gesetzt ist.
	 * @since 0.1
	 */
	private static function getAllowFileUser($file,$user,$module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `allow` FROM `filePermissionsUser` WHERE `file`=:file AND `user`=:user AND `module`=:module");
		$data = $DB->execute(Array("file"=>$file,"user"=>$user,"module"=>$module));
		//Zugriff erlaubt, verboten, nicht festgelegt
		if (count($data)>0)
			return (boolean) $data[0]['allow'];
		else
			return NULL;
	}
	
	/**
	 * -Standard Benutzerzugriff überprüfen-
	 * @param integer $user BenutzerID
	 * @param integer $module ModulID
	 * @return boolean Zugriffserlaubnis/-verbot oder NULL wenn keine Permission gesetzt ist.
	 * @since 0.1
	 */
	private static function getAllowDefaultPermissionsUser($user,$module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `allow` FROM `defaultPermissionsUser` WHERE `user`=:user AND `module`=:module");
		$data = $DB->execute(Array("user"=>$user,"module"=>$module));
		//Zugriff erlaubt, verboten, nicht festgelegt
		if (count($data)>0)
			return (boolean) $data[0]['allow'];
		else
			return NULL;
	}
	
	/**
	 * -Standard Gruppenzugriff überprüfen-
	 * @param integer $group GruppenID
	 * @param integer $module ModulID
	 * @return boolean Zugriffserlaubnis/-verbot oder NULL wenn keine Permission gesetzt ist.
	 * @since 0.1
	 */	
	private static function getAllowDefaultPermissionsGroup($group,$module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `allow` FROM `defaultPermissionsGroup` WHERE `group`=:group AND `module`=:module");
		$data = $DB->execute(Array("group"=>$group,"module"=>$module));
		//Zugriff erlaubt, verboten, nicht festgelegt
		if (count($data)>0)
			return (boolean) $data[0]['allow'];
		else
			return NULL;
	}	

	/**
	 * -Gruppenzugriff auf Ordner festlegen
	 * @param integer $folderID OrdnerID
	 * @param integer $groupID GruppenID
	 * @param integer $moduleID ModulID
	 * @param boolean $allow Zugriffsberechtigung
	 * @since 0.1
	 */
	public static function setAllowFolderGroup($folderID,$groupID,$moduleID,$allow) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `folderPermissionsGroup` (`allow`,`folder`,`group`,`module`) VALUES (:allow,:folder,:group,:module) 
		ON DUPLICATE KEY UPDATE `folderPermissionsGroup` SET `allow`=:allow WHERE `folder`=:folder AND `group`=:group AND `module`=:module");
		$DB->execute(Array("folder"=>$folderID,"group"=>$groupID,"module"=>$moduleID,"allow"=>$allow));
	}
	/**
	 * -Benutzerzugriff auf Ordner festlegen
	 * @param integer $folderID OrdnerID
	 * @param integer $userID BenutzerID
	 * @param integer $moduleID ModulID
	 * @param boolean $allow Zugriffsberechtigung
	 * @since 0.1
	 */
	public static function setAllowFolderUser($folderID,$userID,$moduleID,$allow) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `folderPermissionsUser` (`allow`,`folder`,`user`,`module`) VALUES (:allow,:folder,:user,:module) 
		ON DUPLICATE KEY UPDATE `folderPermissionsUser` SET `allow`=:allow WHERE `folder`=:folder AND `user`=:user AND `module`=:module");
		$DB->execute(Array("folder"=>$folderID,"user"=>$userID,"module"=>$moduleID,"allow"=>$allow));
	}	
	/**
	 * -Gruppenzugriff auf Datei festlegen
	 * @param integer $fileID DateiID
	 * @param integer $groupID GruppenID
	 * @param integer $moduleID ModulID
	 * @param boolean $allow Zugriffsberechtigung
	 * @since 0.1
	 */	
	public static function setAllowFileGroup($fileID,$groupID,$moduleID,$allow) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `filePermissionsGroup` (`allow`,`file`,`group`,`module`) VALUES (:allow,:file,:group,:module) 
		ON DUPLICATE KEY UPDATE `filePermissionsGroup` SET `allow`=:allow WHERE `file`=:file AND `group`=:group AND `module`=:module");
		$DB->execute(Array("file"=>$fileID,"group"=>$groupID,"module"=>$moduleID,"allow"=>$allow));
	}
	/**
	 * -Benutzerzugriff auf Datei festlegen
	 * @param integer $fileID DateiID
	 * @param integer $userID BenutzerID
	 * @param integer $moduleID ModulID
	 * @param boolean $allow Zugriffsberechtigung
	 * @since 0.1
	 */		
	public static function setAllowFileUser($fileID,$userID,$moduleID,$allow) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `filePermissionsUser` (`allow`,`file`,`user`,`module`) VALUES (:allow,:file,:user,:module) 
		ON DUPLICATE KEY UPDATE `filePermissionsUser` SET `allow`=:allow WHERE `file`=:file AND `user`=:user AND `module`=:module");
		$DB->execute(Array("file"=>$fileID,"user"=>$userID,"module"=>$moduleID,"allow"=>$allow));
	}	
	/**
	 * -Standard Gruppenzugriff festlegen
	 * @param integer $groupID GruppenID
	 * @param integer $moduleID ModulID
	 * @param boolean $allow Zugriffsberechtigung
	 * @since 0.1
	 */	
	public static function setAllowDefaultPermissionsGroup($groupID,$moduleID,$allow) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `defaultPermissionsGroup` (`allow`,`group`,`module`) VALUES (:allow,:group,:module) 
		ON DUPLICATE KEY UPDATE `defaultPermissionsGroup` SET `allow`=:allow WHERE `group`=:group AND `module`=:module");
		$DB->execute(Array("group"=>$groupID,"module"=>$moduleID,"allow"=>$allow));
	}
	/**
	 * -Standard Benutzerzugriff festlegen
	 * @param integer $userID BenutzerID
	 * @param integer $moduleID ModulID
	 * @param boolean $allow Zugriffsberechtigung
	 * @since 0.1
	 */		
	public static function setAllowDefaultPermissionsUser($userID,$moduleID,$allow) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `defaultPermissionsUser` (`allow`,`user`,`module`) VALUES (:allow,:user,:module) 
		ON DUPLICATE KEY UPDATE `defaultPermissionsUser` SET `allow`=:allow WHERE `user`=:user AND `module`=:module");
		$DB->execute(Array("user"=>$userID,"module"=>$moduleID,"allow"=>$allow));
	}	
	
}
?>