<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Dateiverwaltung
 * 
 * @author Sascha Sauermann
 * @since 0.1
 *        
 *        
 */
class File {
	/**
	 * Dateipfad
	 * @var String
	 * @since 0.1
	 */
	private $dir;
	/**
	 * Dateiname
	 * @var String
	 * @since 0.1
	 */
	private $filename;
	/**
	 * Datenbankobjekt
	 * @var Database
	 * @since 0.1
	 */
	private $Database;
	
	/**
	 * -Konstruktor-
	 * 
	 * Erstellt eine neue Datenbankverbindung, legt den Pfad und Namen der aktuellen Datei fest
	 * [und erstellt gegebenenfalls eine neue Datei]
	 * 
	 * @param String $dir Pfad der Datei [Mit schließendem Slash]
	 * @param String $filename Dateiname
	 * @param boolean $create [optional=FALSE] Wenn TRUE wird eine neue Datei angelegt.
	 * @since 0.1
	 * @throws Exceptions\NotFileException
	 * @throws Exceptions\AccessDeniedException
	 * @throws Exceptions\IOException
	 * @throws Exceptions\FileAlreadyExistsException
	 * @throws Exceptions\FileNotFoundException
	 */
	public function __construct($dir,$filename,$create=FALSE) {
		//Datenbankverbindung herstellen
		$this->Database = $GLOBALS['Database'];
		//Überprüfen, ob Datei vorhanden ist
		$existing = file_exists($dir.$filename);
		
		if (!is_file($dir.$filename))
			throw new Exceptions\NotFileException($dir.$filename);
		
		/*
		 * Überprüfen, auf:
		 * 		Datei nicht vorhanden && Datei erstellen
		 * 		Datei vorhanden && Datei nicht erstellen
		 * 		Datei vorhanden && erstellen
		 * 		Datei nicht vorhanden && nicht erstellen [else]
		 */
		//Datei nicht vorhanden && Datei erstellen
		if (($existing==FALSE && $create == TRUE)) {
			//Schreibzugriff überprüfen
			if (!is_writable($dir.$filename))
				throw new Exceptions\AccessDeniedException($dir.$filename, Exceptions\AccessDeniedException::WRITE);
			//Erstelle neue Datei			
			$fhandle = fopen($dir.$filename, "a");
			if ($fhandle==FALSE)
				throw new Exceptions\IOException("Can not open file '" . $dir . $filename . "'");
			$closeSuccess = fclose($fhandle);
			if ($closeSuccess==FALSE)	
				throw new Exceptions\IOException("Can not close filehandler for file '" . $dir . $filename . "'");
			$this->dir = $dir;
			$this->filename = $filename;
		}
		//Datei vorhanden && Datei nicht erstellen
		elseif ($existing==TRUE && $create == FALSE)
		{
			//Attribute setzen
			$this->dir = $dir;
			$this->filename = $filename;
		}
		//Datei vorhanden && erstellen
		elseif ($existing==TRUE && $create == TRUE)
		{
			throw new Exceptions\FileAlreadyExistsException($dir.$filename);
		}
		//Datei nicht vorhanden && nicht erstellen
		else
		{
			throw new Exceptions\FileNotFoundException($dir.$filename);
		}
	}
	
	/**
	 * -Destruktor-
	 * Gibt alle Attribute wieder frei
	 * @since 0.1
	 */
	public function __destruct() {
		$this->dir = NULL;
		$this->filename = NULL;
		$this->Database = NULL;
	}
	
	/**
	 * -Datei löschen-
	 * Löscht die Datei und ggf. ihren Datenbankeintrag
	 * @param boolean $delDBEntry [optional=TRUE] Wenn WAHR wird ein evtl. vorhandener Datenbankeintrag mit gelöscht.
	 * @since 0.1
	 * @throws Exceptions\AccessDeniedException
	 * @throws Exceptions\IOException
	 */
	public function delete($delDBEntry=TRUE) {
		//Schreibzugriff überprüfen (Datei)
		if (!is_writable($this->dir.$this->filename))
			throw new Exceptions\AccessDeniedException($this->dir.$this->filename, Exceptions\AccessDeniedException::WRITE);
		//Schreibzugriff überprüfen (Ordner)
		if (!is_writable($this->dir))
			throw new Exceptions\AccessDeniedException($this->dir, Exceptions\AccessDeniedException::WRITE);
		//Datei löschen
		if (unlink($this->dir.$this->filename))
		{
			//Datenbankeintrag ggf. löschen
			if ($delDBEntry===TRUE) {
				$fileID = File::getFileID($this->dir, $this->filename);
				if ($fileID!==NULL) {
					File::deleteDatabaseEntry($fileID);
				}
			}
			$this->dir=NULL;
			$this->filename=NULL;
		}
		else
		{
			throw new Exceptions\IOException("Can not delete file '".$this->dir.$this->filename."'");		
		}
	}
	
	/**
	 * -Datei verschieben-
	 * Verschiebt eine Datei und aktualisiert ggf. ihren Datenbankeintrag.
	 * @param String $destination Zielorder der Datei
	 * @param boolean $overwrite [optional=FALSE] Soll eine gleichnamige Datei im Zielorder ggf. überschrieben werden?
	 * @since 0.1
	 * @throws Exceptions\FileAlreadyExistsException
	 * @throws Exceptions\DirectoryNotFound
	 * @throws Exceptions\AccessDeniedException
	 * @throws Exceptions\IOException
	 */
	public function move($destination,$overwrite=FALSE) {
		//Wenn Überschreiben = Falsch und die Datei beim Ziel bereits existiert, Abbruch
		if ($overwrite == FALSE && file_exists($destination.$this->filename)) {
			throw new Exceptions\FileAlreadyExistsException($destination.$this->filename);
		}
		else
		{
			//Existenz des Zielverzeichnisses überprüfen
			if (!file_exists($destination))
				throw new Exceptions\DirectoryNotFound($destination);
				
			//@TODO Braucht ein Kopier-/Verschiebvorgang Ordnerrechte o.O
			//Schreibzugriff überprüfen (Ordner)
			if (!is_writable($this->dir))
				throw new Exceptions\AccessDeniedException($this->dir, Exceptions\AccessDeniedException::WRITE);
				
			//Datei verschieben
			if(rename($this->dir.$this->filename, $destination.$this->filename))
			{
				$fileID = File::getFileID($this->dir, $this->filename);
				
				//Auf vorhandenen Datenbankeintrag prüfen
				if ($fileID!==NULL) {
					
					//Überprüfen, ob ein anderer Datenbankeintrag mit dem neuen Pfad/Namen existiert.
					$fileIDOverwrite = File::getFileID($destination, $this->filename);
					
					//Wenn ja, dann diesen vorher löschen. (Kann beim Überschreiben passieren)
					if ($fileIDOverwrite!==NULL)
						File::deleteDatabaseEntry($fileIDOverwrite);
					
					//Datenbankeintrag aktualisieren
					try 
					{
					File::updateDatabaseEntry($fileID,$destination,$this->filename);
					}
					catch (Exceptions\FileEntryNotFoundException $ex)
					{ 
						//Datenbankeintrag erstellen und nochmal versuchen zu aktualisieren.
						$this->createDatabaseEntry();
						File::updateDatabaseEntry($fileID,$destination,$this->filename);
					}
				}
				
				//Pfad der Datei auf Zielpfad setzen
				$this->dir=$destination;
			}
			else 
			{
				//Verschieben fehlgeschlagen
				throw new Exceptions\IOException("Can not move file '".$this->dir.$this->filename."' to '".$destination.$this->filename."'");
			}
			
		}
	}
	/**
	 * -Datei kopieren-
	 * Kopiert eine Datei und übernimmt ggf. ihren Datenbankeintrag. Wenn die Datei überschrieben wird,
	 * wird der Datenbankeintrag NICHT aktualisiert.
	 * @param String $destination Zielorder der Kopie
	 * @param boolean $overwrite [optional=FALSE] Soll eine gleichnamige Datei im Zielorder ggf. überschrieben werden?
	 * @since 0.1
	 * @throws Exceptions\FileAlreadyExistsException
	 * @throws Exceptions\DirectoryNotFound
	 * @throws Exceptions\AccessDeniedException
	 * @throws Exceptions\IOException
	 */
	public function copy($destination,$overwrite=FALSE)
	{
		//Wenn Überschreiben = Falsch und die Datei beim Ziel bereits existiert, Abbruch
		if ($overwrite == FALSE && file_exists($destination.$this->filename)) {
			throw new Exceptions\FileAlreadyExistsException($destination.$this->filename);
		}
		else
		{
			//Existenz des Zielverzeichnisses überprüfen
			if (!file_exists($destination))
				throw new Exceptions\DirectoryNotFound($destination);
				
			//@TODO Braucht ein Kopier-/Verschiebvorgang Ordnerrechte o.O
			//Schreibzugriff überprüfen (Ordner)
			if (!is_writable($this->dir))
				throw new Exceptions\AccessDeniedException($this->dir, Exceptions\AccessDeniedException::WRITE);
				
			if (copy($this->dir.$this->filename, $dest.$this->filename)) {
							
				//Berechtigungen der Datei mitkopieren
				$fileID = File::getFileID($this->dir, $this->filename);
				if ($fileID!==NULL) {
					//Kopie der Datei verwenden
					$copyFile = new File($destination, $this->filename);
					//Datenbankeintrag für die Kopie anlegen
					$copyFileID = $copyFile->createDatabaseEntry();
					//Berechtigungen für filePermissionsUser kopieren
					$this->Database->prepareStatement("INSERT INTO `filepermissionsuser` (`file`, `user`, `module`, `allow`)
					 SELECT :newID, `user`, `module`, `allow` FROM  `filepermissionsuser` WHERE `file`=:oldID");
					$data = $this->Database->execute(Array("newID"=>$copyFileID,"oldID"=>$fileID));
					//Berechtigungen für filePermissionsGroup kopieren
					$this->Database->prepareStatement("INSERT INTO `filepermissionsgroup` (`file`, `group`, `module`, `allow`) SELECT
					 :newID, `group`, `module`, `allow` FROM  `filepermissionsgroup` WHERE `file`=:oldID");
					$data = $this->Database->execute(Array("newID"=>$copyFileID,"oldID"=>$fileID));
				}
			}
			else
			{
				//Kopieren fehlgeschlagen
				throw new Exceptions\IOException("Can not copy file '".$this->dir.$this->filename."' to '".$destination.$this->filename."'");
			}
		}
	}
	/**
	 * -Datei umbenennen-
	 * Benennt eine Datei um und aktualisiert ggf. ihren Datenbankeintrag.
	 * @param String $newName Neuer Name der Datei
	 * @param boolean $overwrite [optional=FALSE] Soll eine gleichnamige Datei im Order ggf. überschrieben werden?
	 * @since 0.1
	 * @throws Exceptions\FileAlreadyExistsException
	 * @throws Exceptions\IOException
	 */
	public function rename($newName,$overwrite=FALSE) {
		//Wenn Überschreiben = Falsch und die Datei beim Ziel bereits existiert, Abbruch
		if ($overwrite == FALSE && file_exists($this->dir.$newName)) {
			throw new Exceptions\FileAlreadyExistsException($this->dir.$newName);
		}
		else
		{
			if (rename($this->dir.$this->filename, $this->dir.$newName)) {
				
				//Auf vorhandenen Datenbankeintrag prüfen
				$fileID = File::getFileID($this->dir, $this->filename);
				if ($fileID!==NULL) {
					//Überprüfen, ob ein anderer Datenbankeintrag mit dem neuen Pfad/Namen existiert.
					$fileIDOverwrite = File::getFileID($this->dir, $newName);
					//Wenn ja, dann diesen vorher löschen. (Kann beim Überschreiben passieren)
					if ($fileIDOverwrite!==NULL) {
						File::deleteDatabaseEntry($fileIDOverwrite);
					}
					//Datenbankeintrag aktualisieren
					try 
					{
					File::updateDatabaseEntry($fileID,$this->dir,$newName);
					}
					catch (Exceptions\FileEntryNotFoundException $ex)
					{ 
						//Datenbankeintrag erstellen und nochmal versuchen zu aktualisieren.
						$this->createDatabaseEntry();
						File::updateDatabaseEntry($fileID,$this->dir,$newName);
					}
				}
				return TRUE;
			}
			else 
			{
				//Umbenennen fehlgeschlagen
				throw new Exceptions\IOException("Can not rename file '".$this->dir.$this->filename."' to '".$this->dir.$newName."'");
			}
		}
	}
	/**
	 * -Dateiinhalt auslesen-
	 * Liest den Inhalt einer Datei aus und gibt ihn zurück.
	 * @return String Inhalt der Datei
	 * @throws Exceptions\AccessDeniedException
	 * @throws Exceptions\IOException
	 * @since 0.1
	 */
	public function read() {
		//Lesezugriff überprüfen
		if (!is_readable($this->dir.$this->filename))
			throw new Exceptions\AccessDeniedException($this->dir.$this->filename, Exceptions\AccessDeniedException::READ);
		//Inhalt auslesen
		$content = file_get_contents($this->dir.$this->filename);
		if ($content==FALSE) {
			throw new Exceptions\IOException("Could not get content of file '".$this->dir.$this->filename."'");
		}
		else
		{
			return $content;
		}
	}
	/**
	 * -Dateiinhalt schreiben-
	 * @param String $content Zu schreibender Text
	 * @param boolean $overwrite [optional=FALSE] Soll die Datei überschrieben werden
	 * @throws Exceptions\AccessDeniedException
	 * @throws Exceptions\IOException
	 * @since 0.1
	 */
	public function write($content,$overwrite=FALSE) {
		//Schreibzugriff überprüfen
		if (!is_writable($this->dir.$this->filename))
			throw new Exceptions\AccessDeniedException($this->dir.$this->filename, Exceptions\AccessDeniedException::WRITE);
		if ($overwrite == TRUE) {
			$fhandle = fopen($this->dir.$this->filename, "ab");
		}
		else 
		{
			$fhandle = fopen($this->dir.$this->filename, "wb");
			if ($fhandle==FALSE)
				throw new Exceptions\IOException("Can not open file '" . $this->dir.$this->filename . "'");
		}
		if (!fwrite($fhandle, $content)>0 && strlen($content)>0)
			throw new Exceptions\IOException("Can not write file '" . $this->dir.$this->filename . "'");
		if(!fflush($fhandle))
			throw new Exceptions\IOException("Can not flush file '" . $this->dir.$this->filename . "'");
		if (!fclose($fhandle))
			throw new Exceptions\IOException("Can not close filehandler for file '" . $this->dir.$this->filename . "'");
	}
	
	/**
	 * -Dateierweiterung-
	 * Fragt die Dateiendung der Datei ab.
	 * @return String Dateiendung
	 * @since 0.1
	 */
	public function getExtension() {
		$info = pathinfo($this->dir.$this->filename);
		if (isset($info['extension'])) {
			return $info['extension'];
		}
		else
		{
			return "";
		}
	}
	
	/**
	 * -Dateigröße-
	 * Gibt die Dateigröße der Datei zurück.
	 * @return integer Dateigröße
	 * @since 0.1
	 * @throws Exceptions\IOException
	 */
	public function getFilesize() {
		//Dateigröße abfragen
		$filesize = filesize($this->dir.$this->filename);
		//Fehler aufgetreten
		if ($filesize===FALSE)
			throw new Exceptions\IOException("Can not get filesize for file '" . $this->dir.$this->filename . "'");
		else
			return $filesize;
	}
	/**
	 * -MimeType-
	 * Gibt den MimeType der Datei zurück
	 * @return MimeType
	 * @since 0.1
	 */
	public function getMimeType() {
		//TODO: mime_content_type ist veraltet!!!!
		return mime_content_type($this->dir.$this->filename);
	}
	
	/**
	 * -Datenbankeintrag erstellen-
	 * Trägt eine Datei in die Datenbank ein, damit Berechtigungen hinzugefügt werden können;
	 * Wenn die Datei bereits einen Eintrag in der Datenbank besitzt, wird die ID zurückgegeben.
	 * @return integer ID der Datei
	 * @since 0.1
	 * @throws Exceptions\DBNoRowsInsertedException
	 */
	public function createDatabaseEntry() {
		$id = File::getFileID($this->dir, $this->filename);
		if($id === NULL) {
			$this->Database->prepareStatement("INSERT INTO `files` (`path`, `filename`) VALUES(:dir,:filename)");
			$data = $this->Database->execute(Array("dir"=>$this->dir,"filename"=>$this->filename));
			//Rückgabe ID des Eintrags
			if (count($data)>0)
				return (integer)$this->Database->lastInsertID("file_ID");
			else
				throw new Exceptions\DBNoRowsInsertedException($this->Database->preparedStatement);
		}
		else
		{
			return $id;
		}
	}
	/**
	 * -DateiID-
	 * Gibt die DateiID zurück.
	 * @param String $dir Ordner der Datei
	 * @param String $filename Dateiname
	 * @return integer DateiID
	 * @since 0.1
	 * @throws Exceptions\FileEntryNotFoundException
	 */
	public static function getFileID($dir,$filename) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `file_ID` FROM `files` WHERE `path`=:dir AND `filename`=:filename");
		$data = $DB->execute(Array("filename"=>$filename,"dir"=>$dir));
		//Rückgabe der ID oder NULL
		if (count($data)>0)
			return (integer)$data[0]['file_ID'];
		else
			throw new Exceptions\FileEntryNotFoundException(NULL,$dir,$filename);
	}
	/**
	 * -LetzterÜberprüfungszeitpunkt-
	 * Gibt den Zeitpunkt der letzten Überprüfung des Datenbankeintrags zurück.
	 * @param integer $fileID DateiID
	 * @return String Timestamp der letzten Überprüfung
	 * @since 0.1
	 * @throws Exceptions\FileEntryNotFoundException
	 */
	public static function getLastChecked($fileID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `lastChecked` FROM `files` WHERE `file_ID`=:ID");
		$data = $DB->execute(Array("ID"=>$fileID));
		//Rückgabe des Zeitpunkts der letzten Überprüfung (Timestamp)
		if (count($data)>0)
			return $data[0]['lastChecked'];
		else
			throw new Exceptions\FileEntryNotFoundException($fileID);
	} 
	/**
	 * -Ordner einer Datei-
	 * Gibt den Ordner zu einer DateiID zurück.
	 * @param integer $fileID DateiID
	 * @return String Ordner der Datei
	 * @throws Exceptions\FileEntryNotFoundException
	 * @since 0.1
	 */
	public static function getDirectory($fileID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `path` FROM `files` WHERE `file_ID`=:ID");
		$data = $DB->execute(Array("ID"=>$fileID));
		//Rückgabe des Ordnerpfades
		if (count($data)>0)
			return $data[0]['path'];
		else
			throw new Exceptions\FileEntryNotFoundException($fileIDt);
	} 
	/**
	 * -Dateiname einer Datei-
	 * Gibt den Dateinamen zu einer DateiID zurück.
	 * @param integer $fileID DateiID
	 * @return String Dateiname
	 * @throws Exceptions\FileEntryNotFoundException
	 * @since 0.1
	 */	
	public static function getFilename($fileID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `filename` FROM `files` WHERE `file_ID`=:ID");
		$data = $DB->execute(Array("ID"=>$fileID));
		//Rückgabe des Dateinamen
		if (count($data)>0)
			return $data[0]['filename'];
		else
			throw new Exceptions\FileEntryNotFoundException($fileID);
	} 
	
	/**
	 * -Datenbankeintrag überprüfen-
	 * Überprüft, ob die Datei zu einem Datenbankeintrag noch vorhanden ist; Wenn nicht, wird der Eintrag gelöscht.
	 * @param integer $fileID DateiID
	 * @since 0.1
	 */
	public static function checkFile($fileID) {
		$dir = File::getDirectory($fileID);
		$filename = File::getFilename($fileID);

		if (file_exists($dir.$filename))
		{
				File::setLastChecked($fileID, time());			
		}
		else
			File::deleteDatabaseEntry($fileID);
		
	}
	
	/**
	 * -Datenbankeintrag löschen-
	 * Löscht den Eintrag der Datei in der Datenbank.
	 * @param integer $fileID DateiID
	 * @since 0.1
	 */
	public static function deleteDatabaseEntry($fileID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("DELETE `files`, `filePermissionsGroup`, `filePermissionsUser`
				 FROM `files` LEFT JOIN `filePermissionsGroup` ON (`files.file_ID`=`filePermissionsGroup.file`) LEFT JOIN `filePermissionsUser` ON (`files.file_ID`=`filePermissionsUser.file`)
				 WHERE `files.file_ID` = :id");
		$DB->execute(Array("id"=>$fileID));
	}
	/**
	 * -Datenbankeintrag aktualisieren-
	 * Löscht den Eintrag der Datei in der Datenbank.
	 * @param integer $fileID Datei ID
	 * @param String $dir Neuer Ordner
	 * @param String $filename Neuer Dateiname
	 * @since 0.1
	 */	

	private static function updateDatabaseEntry($fileID,$dir,$filename) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("UPDATE `files` SET `path`=:dir AND `filename`:filename WHERE `fileID`=:ID");
		$DB->execute(Array("ID"=>$fileID,"dir"=>$dir,"filename"=>$filename));
	}
	
	/**
	 * -Letzten Überprüfungszeitpunkt setzen-
	 * Legt den Zeitpunkt der letzten Überprüfung fest.
	 * @param integer $fileID DateiID
	 * @param String $time Zeitpunkt (Timestamp)
	 * @since 0.1
	 * @throws Exceptions\FileEntryNotFound
	 */
	private static function setLastChecked($fileID,$time) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("UPDATE `files` SET `lastChecked`=:time WHERE `fileID`=:ID");
		$DB->execute(Array("ID"=>$fileID,"time"=>$time));
	} 
	
}

?>