<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Benutzerverwaltung
 * 
 * @author Sascha Sauermann
 * @since 0.1 
 */
use OpenFTP\Classes\Exceptions\UserNotFoundException;

class User {
	/**
	 * BenutzerID
	 * @var integer
	 * @since 0.1
	 */
	private $userID;
	/**
	 * Datenbankobjekt
	 * @var Database
	 * @since 0.1
	 */
	private $Database;
	
	/**
	 * -Konstruktor-
	 * 
	 * Erstellt eine neue Datenbankverbindung,
	 * legt $userID auf die ID des aktuell verwalteten Benutzers fest
	 * [und erstellt gegebenenfalls einen neuen Benutzer mit dem übergebenen Namen]
	 * 
	 * @param String $userName Name des Benutzers, die verwaltet werden soll.
	 * @param boolean $create [optional=FALSE] Wenn WAHR wird versucht einen neuen Benutzer mit dem Namen $userName anzulegen.
	 * @since 0.1
	 * @throws Exceptions\UserAlreadyExistsException
	 * @throws Exceptions\UserNotFoundException
	 */
	public function __construct($userName, $create = FALSE) 
	{
		//Datenbankverbindung herstellen
		$this->Database = $GLOBALS['Database'];
		//Datenbank auf Vorkommen mit Benutzernamen überprüfen
		$this->Database->prepareStatement("SELECT COUNT(*) FROM users WHERE username = :user");
		$data = $this->Database->execute(Array("user"=>$userName));
		/*
		 * Überprüfen, auf:
		 * 		Benutzer nicht vorhanden && Benutzer erstellen
		 * 		Benutzer vorhanden && Benutzer nicht erstellen
		 * 		Benutzer vorhanden && erstellen || Benutzer nicht vorhanden && nicht erstellen [else]
		 */
		if (($data[0]['COUNT(*)']=="0" && $create == TRUE)) {
			//Erstelle neuen Benutzer
			$this->Database->prepareStatement("INSERT INTO users (`username`) VALUES (:user)");
			$data = $this->Database->execute(Array("user"=>$userName));
			//Neue ID $userID zuweisen
			$this->userID = $this->Database->lastInsertID("user_ID");
		}
		elseif ($data[0]['COUNT(*)']=="1" && $create == FALSE)
		{
			//Frage ID des Benutzers mit $userName ab
			//Neue ID $userID zuweisen
			$this->userID = User::getUserID($userName);
		}
		else
		{
			//Benutzer anlegen: Benutzer mit gleichem Namen bereits vorhanden
			if ($create==TRUE) {
				throw new Exceptions\UserAlreadyExistsException($userName);			
			}
			//Benutzer "benutzen": Benutzer nicht vorhanden
			else 
			{
				throw new Exceptions\UserNotFoundException($userName);
			}
		}
	}
	
	/**
	 * -Destruktor-
	 * Gibt alle Attribute wieder frei
	 * @since 0.1
	 */
	public function __destruct() {
		$this->userID = NULL;
		$this->Database = NULL;
	}
	
	/**
	 * -Benutzer löschen-
	 * Entfernt den verwalteten Benutzer aus der Datenbank.
	 * @since 0.1
	 */
	public function remove() {
		//Benutzer aus jeder Tabelle löschen, in der er verwendet wird.
		$this->Database->prepareStatement("DELETE `users`, `userGroups`, `defaultPermissionsUser`, `filePermissionsUser`, `folderPermissionsUser`
				 FROM `users` LEFT JOIN `userGroups` ON (`users.user_ID`=`userGroups.user`) LEFT JOIN `defaultPermissionsUser` ON (`users.user_ID`=`defaultPermissionsUser.user`)
				 LEFT JOIN `filePermissionsUser` ON (`users.user_ID`=`filePermissionsUser.user`) LEFT JOIN `folderPermissionsUser` ON (`users.user_ID`=`folderPermissionsUser.user`)
				 WHERE `users.user_ID` = :id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		//@TODO: Fehler, wenn nichts gelöscht wurde (FRAGEZEICHEN)
		//if (!count($data)>0)
		//	throw new Exceptions\UserNotFoundException();
	}
		
	/**
	 * -Benutzername festlegen-
	 * Setzt den Namen des verwalteten Benutzers auf $name fest.
	 * @param String $name Festzulegender Benutzername
	 * @since 0.1
	 */
	public function setUsername($name) {
		$this->Database->prepareStatement("UPDATE `users` SET `username`=:name WHERE `user_ID` =:id");
		$this->Database->execute(Array("id"=>$this->userID,"name"=>$name));
	}
	
	/**
	 * -Passwort festlegen-
	 * Setzt das Passwort des verwalteten Benutzers auf den MD5-Hash von $password fest.
	 * @param String $password Festzulegendes Passwort
	 * @since 0.1
	 */
	public function setPassword($password) {
		$this->Database->prepareStatement("UPDATE `users` SET `password`=:password WHERE `user_ID` =:id");
		$this->Database->execute(Array("id"=>$this->userID,"password"=>md5($password)));
	}
	
	/**
	 * -Passwort abfragen-
	 * Gibt den MD5-Hash des Passworts des verwalteten Benutzers zurück.
	 * @return String MD5-Hash des Passworts
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */	
	public function getPassword() {
		$this->Database->prepareStatement("SELECT `password` FROM `users` WHERE `user_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		//Rückgabe des Passworts
		if (count($data)>0)
			return $data[0]['password'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$this->userID);
	}
	
	/**
	 * -Email festlegen-
	 * Setzt die Email des verwalteten Benutzers auf $email fest.
	 * @param String $email Festzulegende Email
	 * @since 0.1
	 */
	public function setEmail($email) {
		$this->Database->prepareStatement("UPDATE `users` SET `email`=:email WHERE `user_ID` =:id");
		$data = $this->Database->execute(Array("id"=>$this->userID,"email"=>$email));
	}
	
	/**
	 * -Email abfragen-
	 * Gibt die Email des verwalteten Benutzers zurück.
	 * @return String Email
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */	
	public function getEmail() {
		$this->Database->prepareStatement("SELECT `email` FROM `users` WHERE `user_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		//Rückgabe der Email
		if (count($data)>0)
			return $data[0]['email'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$this->userID);
	}
	
	/**
	 * -Letzten Login festlegen-
	 * Setzt den Zeitpunkt des letzten Logins des verwalteten Benutzers auf $lastLogin fest.
	 * @param String $lastLogin Festzulegender Zeitpunkt (Timestamp)
	 * @since 0.1
	 */
	public function setLastLogin($lastLogin) {
		$this->Database->prepareStatement("UPDATE `users` SET `lastLogin`=:lastLogin WHERE `user_ID` =:id");
		$this->Database->execute(Array("id"=>$this->userID,"lastLogin"=>$lastLogin));
	}
	
	/**
	 * -Letzten Login abfragen-
	 * Gibt den Zeitpunkt des letzten Logins des verwalteten Benutzers zurück.
	 * @return String Zeitpunkt (Timestamp)
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */	
	public function getLastLogin() {
		$this->Database->prepareStatement("SELECT `lastLogin` FROM `users` WHERE `user_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		//Rückgabe des letzten Logins
		if (count($data)>0)
			return $data[0]['lastLogin'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$this->userID);
	}
	
	/**
	 * -Registrierungszeitpunkt festlegen-
	 * Setzt den Zeitpunkt der Registrierung des verwalteten Benutzers auf $regDate fest.
	 * @param String $regDate Festzulegender Zeitpunkt (Timestamp)
	 */
	public function setRegDate($regDate) {
		$this->Database->prepareStatement("UPDATE `users` SET `regDate`=:regDate WHERE `user_ID` =:id");
		$this->Database->execute(Array("id"=>$this->userID,"regDate"=>$regDate));
	}
	
	/**
	 * -Registrierungsdatum abfragen-
	 * Gibt den Zeitpunkt der Registrierung des verwalteten Benutzers zurück.
	 * @return String Zeitpunkt (Timestamp)
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */	
	public function getRegDate() {
		$this->Database->prepareStatement("SELECT `regDate` FROM `users` WHERE `user_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		if (count($data)>0)
			return $data[0]['regDate'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$this->userID);
	}
	
	/**
	 * -Gruppen auflisten-
	 * Gibt ein Array mit den Gruppen des aktuell verwalteten Benutzers zurück.
	 * @return Array Numerisches Array mit den Gruppen
	 * @since 0.1
	 */
	public function getGroups() {
		$this->Database->prepareStatement("SELECT `group` FROM `userGroups` WHERE `user` =:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));

		//Gruppen in ein numerisches Array einsortieren
		$return = Array();
		foreach ($data as $value)
		{
			array_push($return, $value['group']);
		}
		return $return;
		
	}
	/**
	 * -Gruppen setzten-
	 * Löscht alle Gruppen des verwalteten Benutzers und legt die angegebenen Gruppen neu fest.
	 * @param Array $groups Numerisches Array mit den ID's der festzulegenden Gruppen.
	 * @since 0.1
	 */
	public function setGroups($groups) {
		//Überprüfen ob der Benutzer noch in keiner Gruppe ist
		if (count($this->getGroups())>0)
		{
			//Gruppen löschen wenn vorhanden
			$this->removeGroups(-1);
		}
		$this->addGroups($groups);
	}
	/**
	 * -Gruppe hinzufügen-
	 * Fügt dem Verwalteten Benutzer eine Gruppe hinzu.
	 * @param Integer $group ID der hinzuzufügenden Gruppe
	 * @throws Exceptions\UserAlreadyInGroupException
	 * @since 0.1
	 */
	public function addGroup($group) {
		//Überprüfen, ob Gruppe existiert (Wenn nicht -> Fehler)
		Group::getName($group);
		//Überprüfen, ob Benutzer nicht in der Gruppe (Wenn nicht -> Fehler)
		if (in_array($group, $this->getGroups()))
			throw new Exceptions\UserAlreadyInGroupException($this->userID, $group);
		$this->Database->prepareStatement("INSERT INTO `userGroups` (`group`, `user`) VALUES(:group,:id)");
		$this->Database->execute(Array("id"=>$this->userID,"group"=>$group));
		
	}
	/**
	 * -Gruppe entfernen-
	 * Entfernt den verwalteten Benutzer aus einer Gruppe.
	 * @param Integer $group ID der zu entfernenden Gruppe
	 * @since 0.1
	 */
	public function removeGroup($group) {
		//Überprüfen, ob Gruppe existiert (Wenn nicht -> Fehler)
		Group::getName($group);
		//Gruppe löschen
		$this->Database->prepareStatement("DELETE FROM `userGroups` WHERE `user`=:id AND `group`=:group");
		$this->Database->execute(Array("id"=>$this->groupID,"group"=>$group));
	}
	/**
	 * -Gruppen hinzufügen-
	 * Fügt dem verwalteten Benutzer Gruppen hinzu.
	 * @param Array $groups Array mit ID's der hinzuzufügenden Gruppen
	 * @since 0.1
	 */
	public function addGroups($groups) {
		//Jeden angegebene Gruppe hinzufügen
		foreach ($groups as $value)
		{
			$this->addGroup($value);
		}
	}
	/**
	 * -Gruppen entfernen-
	 * Entfernt Gruppen vom verwalteten Benutzer.
	 * @param Array|Integer $groups Array mit ID's der zu entfernenden Gruppen. Oder -1 um alle Gruppen zu entfernen.
	 * @since 0.1
	 */
	public function removeGroups($groups) {
		//Alle Gruppen des Benutzers entfernen (bei z.B. -1)
		if ($groups<0) {
			$this->Database->prepareStatement("DELETE FROM `userGroups` WHERE `user`=:id");
			$this->Database->execute(Array("id"=>$this->userID));
		}
		//Bestimmte Gruppen entfernen
		else
		{
			//Jeden übergebenen Benutzer löschen.
			foreach ($groups as $value)
			{
				//Gruppe entfernen
				$this->removeGroup($value);
			}
		}
	
	}
	/**
	 * -Zeit in lokale Zeit konvertieren-
	 * Konvertiert den angegebenen Timestamp in die lokale Zeit des Benutzers unter Berücksichtigung der Zeitzone und der Sommerzeit.
	 * @param String $time [optional] Timestamp Standard: time()
	 * @return String Timestamp der lokalen Zeit, bei nicht gesetzter Zeitzone UTC Zeit
	 */
	public function getLocalTime($time=NULL) {
		$timezone = $this->getTimezone();
		
		//Wenn keine Zeit angegeben wurde, aktuelle Zeit berechnen.
		if ($time==NULL) {
			$time=time();
		}
			
		if ($timezone==0)
			return time();
		else
		{
			//Angegebene UTC Zeit an aktuelle Zeitzone anpassen:
			//+1h pro Zeitzone (ggf. Minus)
			//+1h bei Sommerzeit
			$timestamp = $time+($timezone+date('I',$time))*3600;
		}
	}
	
	/**
	 * -Zeitzone abfragen-
	 * Fragt die zeitzone des Benutzers ab
	 * @return integer Zeitzone
	 * @throws Exceptions\UserNotFoundException
	 * @since 0.1
	 */
	public function getTimezone() {
		$this->Database->prepareStatement("SELECT `timezone` FROM `users` WHERE `user_ID` =:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		if (count($data)>0)
			return (integer) $data[0]['timezone'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$this->userID);
	}
	
	/**
	 * -Zeitzone festlegen-
	 * Legt die Zeitzone des Benutzers fest.
	 * @param integer $timezone
	 * @since 0.1
	 */
	public function setTimezone($timezone) {
		$this->Database->prepareStatement("UPDATE `users` SET `timezone`=:tz WHERE `user_ID` =:id");
		$this->Database->execute(Array("id"=>$this->userID,"tz"=>$timezone));
	}
	
	/**
	 * -Sprache abfragen-
	 * Fragt die Sprache ab, in der dem Benutzer die Seite angezeigt wird
	 * @return String Sprache des Users
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */
	public function getLanguage() {
		$this->Database->prepareStatement("SELECT `language` FROM `users` WHERE `user_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->userID));
		//Rückgabe der Sprache
		if (count($data)>0)
			return $data[0]['language'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$this->userID);
	}
	
	/**
	 * -Sprache festlegen-
	 * Legt die Sprache fest in der dem Benutzer die Seite angezeigt wird.
	 * @param String $lang Festzulegende Sprache
	 * @since 0.1
	 */
	public function setLanguage($lang) {
		$this->Database->prepareStatement("UPDATE `users` SET `language`=:lang WHERE `user_ID` =:id");
		$this->Database->execute(Array("id"=>$this->userID,"lang"=>$lang));
	}
	
	/**
	 * -Benutzername zu ID-
	 * Fragt den Namen eines Benutzers zu einer ID ab.
	 * @param integer $userID ID des Benutzers
	 * @return String Name des Benutzers
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */
	public static function getName($userID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `username` FROM `users` WHERE `user_ID`=:id");
		$data = $DB->execute(Array("id"=>$userID));
		//Rückgabe des Namens
		if (count($data)>0)
			return $data[0]['username'];
		else
			throw new Exceptions\UserNotFoundException(NULL,$userID);
	}

	/**
	 * -Vorhandene Benutzer-
	 * Fragt die vorhandenen Benutzer ab.
	 * @return Array Numerisches Array mit den ID's der vorhandenen Benutzer.
	 * @since 0.1
	 */
	public static function getUsers() {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `user_ID` FROM `users`");
		$data = $DB->execute();
		//Rückgabe des Arrays mit den Benutzern
		if (count($data)>0)
		{
			//Rückgabe der Abfrage umformen
			$return = Array();
			foreach ($data as $value)
			{
				array_push($return, $value);
			}
			return $return;
		}
		else
			return Array();
	}
	/**
	 * -ID zu Username-
	 * @param String $userName Username des Benutzers
	 * @return integer ID des Benutzers
	 * @since 0.1
	 * @throws Exceptions\UserNotFoundException
	 */
	public static function getUserID($userName) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `user_ID` FROM `users` WHERE `username`=:name");
		$data = $DB->execute(Array("name"=>$userName));
		//Rückgabe der ID
		if (count($data)>0)
			return (integer) $data[0]['user_ID'];
		else
			throw new Exceptions\UserNotFoundException($userName);
	}

	/**
	 * -Angemeldeter Benutzer-
	 * Fragt die ID des angemeldeten Benutzers aus der Session ab.
	 * @return integer ID des Benutzers
	 * @since 0.1
	 */
	public static function getSessionID() {
		if (isset($_SESSION['USER']) && $_SESSION['USER']!="")
			return (integer) $_SESSION['USER'];
		else
			return 0;	
	}
	
	/**
	 * -Benutzer einloggen-
	 * Loggt den Benutzer ein, wenn das Passwort stimmt, oder wirft einen Fehler.
	 * @param String $pass Passwort
	 * @throws Exceptions\LogicException
	 * @since 0.1
	 */
	public function login($pass)
	{
		if ($this->getPassword() == md5($pass))
			$_SESSION['USER']=$this->userID;
		else
			throw new Exceptions\LogicException(Exceptions\LogicException::LOGIN_FAILED);
	}
}
?>