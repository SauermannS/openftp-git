<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Modulverwaltung
 * 
 * @author Sascha Sauermann
 * @since 0.1 
 */

class Module {
	/**
	 * ModulID
	 * @var integer
	 * @since 0.1
	 */
	private $moduleID;
	/**
	 * Datenbankobjekt
	 * @var Database
	 */
	private $Database;

	
	/*
	 * Demotypen für Module
	 */
	const TYPE_FILEBROWSER=1;
	const TYPE_ADMIN=2;
	const TYPE_USER=3;
	
	/**
	 * -Konstruktor-
	 * 
	 * Erstellt eine neue Datenbankverbindung und legt $moduleID auf die ID des aktuell verwalteten Moduls fest.
	 * 
	 * @param String $moduleName Name des Moduls
	 * @since 0.1
	 */
	public function __construct($moduleName) 
	{
		$this->Database = $GLOBALS['Database'];
		//Datenbank auf Vorkommen mit Modulnamen überprüfen
		//$this->Database->prepareStatement("SELECT COUNT(*) FROM modules WHERE modulename = :module");
		//$data = $this->Database->execute(Array("module"=>$moduleName));
		//if ($data[0]['COUNT(*)']=="1" ) 
		/*getModuleID wirft schon einen Fehler, wenn das Modul nicht vorhanden ist*/
		$this->moduleID=Module::getModuleID($moduleName);
		//else
		//	throw new Exceptions\ModuleNotFoundException(NULL,$moduleName);
	}
	
	/**
	 * -Destruktor-
	 * Gibt alle Attribute wieder frei
	 * @since 0.1
	 */
	public function __destruct() {
		$this->moduleID = NULL;
		$this->Database = NULL;
	}
	
	/**
	 * -Typ festlegen-
	 * Setzt den Typ des verwalteten Moduls auf $type fest.
	 * @param String $type Festzulegender Typ
	 * @since 0.1
	 */
	public function setType($type) {
		$this->Database->prepareStatement("UPDATE `modules` SET `type`=:type WHERE `module_ID` =:id");
		$this->Database->execute(Array("id"=>$this->moduleID,"type"=>$type));
	}
	
	/**
	 * -Typ abfragen-
	 * Gibt den Typ des Moduls zurück.
	 * @return String Typ des Moduls
	 * @since 0.1
	 * @throws Exceptions\ModuleNotFoundException
	 */	
	public function getType() {
		$this->Database->prepareStatement("SELECT `type` FROM `modules` WHERE `modules_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->moduleID));
		//Rückgabe des Typs
		if (count($data)>0)
			return $data[0]['type'];
		else
			throw new Exceptions\ModuleNotFoundException($this->moduleID);
	}
	
	/**
	 * -Status festlegen-
	 * Setzt den Status des verwalteten Moduls auf $isActive fest.
	 * @param boolean $isActive Festzulegender Status
	 * @since 0.1
	 */
	public function setIsActive($isActive) {
		$this->Database->prepareStatement("UPDATE `modules` SET `active`=:isActive WHERE `module_ID` =:id");
		$this->Database->execute(Array("id"=>$this->moduleID,"isActive"=>(integer)$isActive));
	}
	
	/**
	 * -Status abfragen-
	 * Gibt den Status des Moduls zurück.
	 * @return boolean Status des Moduls
	 * @since 0.1
	 * @throws Exceptions\ModuleNotFoundException
	 */	
	public function getIsActive() {
		$this->Database->prepareStatement("SELECT `active` FROM `modules` WHERE `module_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->moduleID));
		//Rückgabe des Typs oder FALSCH
		if (count($data)>0)
			return (boolean) $data[0]['active'];
		else
			throw new Exceptions\ModuleNotFoundException($this->moduleID);
	}
	/**
	 * -Standard Berechtigung festlegen-
	 * Setzt die Standard Berechtigung des verwalteten Moduls auf $defPermission fest.
	 * @param boolean $defPermission Festzulegende Permission
	 * @since 0.1
	 */
	public function setDefaultPermission($defPermission) {
		$this->Database->prepareStatement("UPDATE `modules` SET `default`=:defPermission WHERE `module_ID` =:id");
		$this->Database->execute(Array("id"=>$this->moduleID,"defPermission"=>$defPermission));
	}
	
	/**
	 * -Standard Berechtigung abfragen-
	 * Gibt die Standard Berechtigung des Moduls zurück.
	 * @return boolean Standard Berechtigung des Moduls
	 * @since 0.1
	 */	
	public function getDefaultPermission() {
		$this->Database->prepareStatement("SELECT `default` FROM `modules` WHERE `module_ID`=:id");
		$data = $this->Database->execute(Array("id"=>$this->moduleID));
		//Rückgabe der DefaultPermission
		if (count($data)>0)
			return (boolean)$data[0]['default'];
		else
			throw new Exceptions\ModuleNotFoundException($this->moduleID);
	}
	
	/**
	 * -Namen festlegen-
	 * Legt den Namen des verwalteten Moduls auf $name fest.
	 * @param String $name Festzulegender Name
	 * @since 0.1
	 */
	public function setName($name) {
		$this->Database->prepareStatement("UPDATE `modules` SET `modulename`=:name WHERE `module_ID` =:id");
		$this->Database->execute(Array("id"=>$this->moduleID,"name"=>$name));
	}
	
	/**
	 * -Name abfragen-
	 * Gibt den Namen eines Moduls zurück.
	 * @param integer $moduleID ID des Moduls
	 * @return String Name des Moduls
	 * @since 0.1
	 * @throws Exceptions\ModuleNotFoundException
	 */	
	public static function getName($moduleID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `modulename` FROM `modules` WHERE `module_ID`=:id");
		$data = $DB->execute(Array("id"=>$moduleID));
		//Rückgabe des Namens oder FALSCH
		if (count($data)>0)
			return $data[0]['modulename'];
		else
			throw new Exceptions\ModuleNotFoundException($this->moduleID);
	}
	
	/**
	 * -ModulID abfragen-
	 * Gibt die ID des Moduls zurück.
	 * @return integer ID des Moduls
	 * @since 0.1
	 */	
	public static function getModuleID($module) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `module_ID` FROM `modules` WHERE `modulename`=:name");
		$data = $DB->execute(Array("name"=>$module));
		if (count($data)>0)
			return (integer) $data[0]['module_ID'];
		else
			throw new Exceptions\ModuleNotFoundException(NULL, $module);
	}

	/**
	 * -Vorhandene Module-
	 * Fragt die vorhandenen Module ab. Wenn type angegeben ist, werden die Module nach diesem Typ gefiltert.
	 * @param integer $type [optional] Typ nach dem die Module gefiltert werden sollen.
	 * @return Array Numerisches Array mit den ID's der vorhandenen Module.
	 * @since 0.1
	 */
	public static function getModules($type=NULL) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$data = Array();
		if ($type==NULL) 
		{
			$DB->prepareStatement("SELECT `module_ID` FROM `modules`");
			$data = $DB->execute();
		}
		else 
		{
			$DB->prepareStatement("SELECT `module_ID` FROM `modules` WHERE `type`=:type");
			$data = $DB->execute(Array("type"=>$type));
		}
		//Rückgabe des Arrays mit den Modulen

		//Rückgabe der Abfrage umformen
		$return = Array();
		foreach ($data as $value)
		{
			array_push($return, $value);
		}
		return $return;
		
	}	
}
?>