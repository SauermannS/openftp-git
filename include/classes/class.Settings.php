<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Verwaltung der Einstellungen.
 * 
 * @author Sascha Sauermann
 * @since 0.1
 */
class Settings {
	
	/**
	 * -Einstellung abfragen-
	 * Fragt den Wert einer Einstellung ab
	 * @param String $setting Einstellung, dessen Wert abgefragt werden soll
	 * @param boolean $ignoreMissing [optional=FALSE] Bestimmt, ob kein Fehler geworfen werden soll, wenn die Einstellung nicht vorhanden ist.
	 * @return String Wert der Einstellung oder NULL
	 * @throws Exceptions\SettingNotFoundException
	 * @since 0.1
	 */
	public static function getSetting($setting, $ignoreMissing=FALSE) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `value` FROM `settings` WHERE `setting`=:setting");
		$data = $DB->execute(Array("setting"=>$setting));
		//Rückgabe der Wertes oder NULL
		if (count($data)>0)
			return $data[0]['value'];
		else
			if ($ignoreMissing==TRUE)
				return NULL;
			else
				throw new Exceptions\SettingNotFoundException($setting);
	}
	/**
	 * -Einstellung setzen-
	 * Legt den Wert einer Einstellung fest
	 * @param String $setting Name der Einstellung
	 * @param String $value Festzulegender Wert
	 * @throws Exceptions\DBNoRowsInsertedException
	 * @since 0.1
	 */
	public static function setSetting($setting, $value) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("INSERT INTO `settings` (`setting`,`value`) VALUES (:setting,:value) 
		ON DUPLICATE KEY UPDATE `settings` SET `value`=:value WHERE `setting`=:setting");
		$data = $DB->execute(Array("setting"=>$setting,"value"=>§value));
		if (!count($data)>0)
			throw new Exceptions\DBNoRowsInsertedException($DB->preparedStatement);
	}
	
	/**
	 * -Einstellung löschen-
	 * Löscht eine Einstellung
	 * @param String $setting Einstellung, die gelöscht werden soll
	 * @since 0.1
	 */
	public static function delSetting($setting, $ignoreMissing=FALSE) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("DELETE FROM `settings` WHERE `setting`=:setting");
		$data = $DB->execute(Array("setting"=>$setting));
		//Rückgabe der Wertes
		if (count($data)>0)
			return $data[0]['value'];

	}
}
?>