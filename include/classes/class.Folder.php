<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Ordnerverwaltung
 * 
 * @author Sascha Sauermann
 * @since 0.1       
 */
use OpenFTP\Classes\Exceptions\IOException;

class Folder 
{
	/**
	 * Ordnerpfad
	 * @var String
	 * @since 0.1
	 */
	private $path;
	/**
	 * Datenbankobjekt
	 * @var Database
	 * @since 0.1
	 */
	private $Database;
	
	/**
	 * -Konstruktor-
	 * Erstellt eine neue Datenbankverbindung, legt den Pfad des aktuellen Ordners fest
	 * [und erstellt ihn gegebenenfalls]
	 * @param String $path Pfad des Ordners
	 * @param boolean $create [optional=FALSE] Soll eine neue Datei erstellt werden
	 * @throws Exceptions\NotDirectoryException
	 * @throws Exceptions\IOException
	 * @throws Exceptions\DirectoryAlreadyExistsException
	 * @throws Exceptions\DirectoryNotFoundException
	 * @since 0.1
	 */
	public function __construct($path,$create=FALSE)
	{
		//Datenbankverbindung herstellen
		$this->Database = $GLOBALS['Database'];
		//Überprüfen, ob Ordner vorhanden ist
		$existing = file_exists($path);
		
		if (!is_dir($path))
			throw new Exceptions\NotDirectoryException($path);
		
		/*
		 * Überprüfen, auf:
		 * 		Ordner nicht vorhanden && Ordner erstellen
		 * 		Ordner vorhanden && Ordner nicht erstellen
		 * 		Ordner vorhanden && erstellen
		 * 		Ordner nicht vorhanden && nicht erstellen [else]
		 */
		//Ordner nicht vorhanden && Ordner erstellen
		if (($existing==FALSE && $create == TRUE)) {
			//Erstelle neuen Ordner
			if(!mkdir($path))
				throw new Exceptions\IOException("Can not create folder '$path'");
			$this->path = $path;
		}
		//Ordner vorhanden && Ordner nicht erstellen
		elseif ($existing==TRUE && $create === FALSE)
		{
			//Attribute setzen
			$this->path = $path;
		}
		//Ordner vorhanden && erstellen
		elseif ($existing==TRUE && $create === TRUE)
		{
			throw new Exceptions\DirectoryAlreadyExistsException($path);
		}
		//Ordner nicht vorhanden && nicht erstellen
		else
		{
			throw new Exceptions\DirectoryNotFoundException($path);
		}
	}

	/**
	 * -Destruktor-
	 * Gibt alle Attribute wieder frei
	 * @since 0.1
	 */
	public function __destruct() 
	{
		$this->path = NULL;
		$this->Database = NULL;
	}

	
	public function delete(){}
	
	public function move($destination){}
	
	public function rename($newName)
	{
		
	}
	
	public function copy($destination){}
	
	public function getSubFolders(){}
	
	public function getFiles(){}
	
	public function getSize(){}
	
	/**
	 * -IsRoot abfragen-
	 * Gibt isRoot zurück.
	 * @return boolean Ist Ordner ein Ausgangsverzeichnis
	 * @throws Exceptions\DirectoryEntryNotFoundException
	 * @since 0.1
	 */
	public function getIsRoot()
	{
		$this->Database->prepareStatement("SELECT `isRoot` FROM `folders` WHERE `path`=:path");
		$data = $this->Database->execute(Array("path"=>$this->path));
		//Boolischen Wert zurückgeben
		if (count($data)>0)
			return (boolean)$data[0]['isRoot'];
		else
			throw new Exceptions\DirectoryEntryNotFoundException(NULL,$this->path);
	}
	
	/**
	 * -IsRoot setzen-
	 * Legt isRoot fest.
	 * @param boolean $isRoot Ist Ordner ein Ausgangsverzeichnis
	 * @since 0.1
	 */
	public function setIsRoot($isRoot)
	{
		$this->Database->prepareStatement("UPDATE `folders` SET `isRoot`=:isRoot WHERE `path`=:path");
		$this->Database->execute(Array("isRoot"=>$isRoot,"path"=>$this->path));
	}
	
	public function createDatabaseEntry(){}
	
	/**
	 * -Datenbankeintrag löschen-
	 * Löscht den Eintrag eines Ordners aus der Datenbank.
	 * @param integer $folderID OrdnerID
	 * @since 0.1
	 */
	public static function deleteDatabaseEntry($folderID)
	{
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("DELETE `folders`, `folderPermissionsGroup`, `folderPermissionsUser`
				 FROM `folders` LEFT JOIN `folderPermissionsGroup` ON (`folders.folder_ID`=`folderPermissionsGroup.folder`) LEFT JOIN `folderPermissionsUser` ON (`folders.folder_ID`=`folderPermissionsUser.folder`)
				 WHERE `folders.folder_ID` = :id");
		$DB->execute(Array("id"=>$folderID));
	}
	
	private static function updateDatabaseEntry()
	{
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("UPDATE `folders` SET `path`=:dir AND `filename`:filename WHERE `fileID`=:ID");
		$DB->execute(Array("ID"=>$fileID,"dir"=>$dir,"filename"=>$filename));
	}
	
	/**
	 * -Ordnername-
	 * Gibt den Namen des Ordners zurück
	 * @return String Ordnername
	 * @since 0.1
	 */
	public function getDirName()
	{
		//Ordnernamen zurückgeben
		return dirname($this->path);
	}
	
	/**
	 * -Pfad zu ID-
	 * Gibt den Pfad eines Ordners zu einer ID zurück.
	 * @param integer $folderID OrdnerID
	 * @return String Pfad des Ordners
	 * @throws Exceptions\DirectoryEntryNotFoundException
	 * @since 0.1
	 */
	public static function getPath($folderID)
	{
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `path` FROM `folders` WHERE `folder_ID`=:ID");
		$data = $DB->execute(Array("ID"=>$folderID));
		//Rückgabe des Ordnerpfads
		if (count($data)>0)
			return $data[0]['path'];
		else
			throw new Exceptions\DirectoryEntryNotFoundException($folderID);
	}
		
	/**
	 * -ID zu Pfad-
	 * Gibt die ID des Ordners in der Datenbank zurück.
	 * @param String $path Pfad des Ordners
	 * @return integer OrdnerID
	 * @throws Exceptions\DirectoryEntryNotFoundException
	 * @since 0.1
	 */
	public static function getFolderID($path)
	{
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `folder_ID` FROM `folders` WHERE `path`=:path");
		$data = $DB->execute(Array("path"=>$path));
		//Rückgabe der ID oder NULL
		if (count($data)>0)
			return (integer)$data[0]['folder_ID'];
		else
			throw new Exceptions\DirectoryEntryNotFoundException(NULL,$path);
	}
	
	/**
	 * -Root Ordner-
	 * Gibt ein Array mit den Root Ordnern zurück.
	 * @return Array RootOrdner
	 * @since 0.1
	 */
	public static function getRootFolders()
	{
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `folder_ID` FROM `folders` WHERE `isRoot`=:isRoot");
		$data = $DB->execute(Array("isRoot"=>$isRoot));
		//Rückgabe der IDs in einem Array
		if (count($data)>0)
		{
			//Rückgabe der Abfrage umformen
			$return = Array();
			foreach ($data as $value)
			{
				array_push($return, $value);
			}
			return $return;
		}
		else
			return Array();
	}
	
	public static function getLastChecked($folderID){}
	
	private static function setLastChecked($folderID){}
	
	public static function checkFolder($folderID){}
}
?>