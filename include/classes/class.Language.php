<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Sprachenverwaltung und für die Mehrsprachigkeit der Seite
 * 
 * @author Sascha Sauermann
 * @since 0.1
 *        
 */
class Language {
	/**
	 * Aktuell verwendete Sprache
	 * @var String
	 */
	private $language;
	/**
	 * Datenbankobjekt
	 * @var Database;
	 */
	private $Database;
	
	/**
	 * -Konstruktor-
	 * 
	 * Erstellt eine neue Datenbankverbindung und legt $language auf die aktuelle Sprache fest.
	 * Wenn keine Sprache angegeben wurde und die Einstellung für die Standardsprache nicht gesetzt ist,
	 * wird Englisch verwendet.
	 * 
	 * @param String $lang [optional] Aktuelle Sprache. Wenn NULL wird die Standardsprache verwendet.
	 * @since 0.1 
	 * @throws Exceptions\LanguageNotFoundException
	 */
	public function __construct($lang=NULL) {
		$this->Database = $GLOBALS['Database'];
		if ($lang==NULL) 
		{
			try 
			{
				$lang = Settings::getSetting('DEFAULT_LANGUAGE');
			}
			catch (Exceptions\SettingNotFoundException $ex)
			{
				$lang="EN";
			}
		}
		if(in_array($lang, Language::getLanguages()))
			$this->language = $lang;
		else 
			throw new Exceptions\LanguageNotFoundException($lang);
	}
	
	/**
	 * -Destruktor-
	 * Gibt alle Attribute wieder frei
	 * @since 0.1
	 */	
	public function __destruct()
	{
		$this->Database=NULL;
		$this->language=NULL;
	}

	/**
	 * -Sprachschnipsel abfragen-
	 * Fragt den Text eines bestimmten Elements der Seite in der aktuellen Sprache ab.
	 * @param String $element Elementname dessen Wert abgefragt werden soll
	 * @since 0.1
	 * @throws Exceptions\LanguageElementNotFoundException
	 */
	public function getValue($element) {
		$this->Database->prepareStatement("SELECT `value` FROM `language` WHERE `language`=:lang AND `element`=:element");
		$data = $this->Database->execute(Array("lang"=>$this->language,"element"=>$element));
		//Rückgabe des Wertes
		if (count($data)>0)
			return $data[0]['value'];
		else
			throw new Exceptions\LanguageElementNotFoundException($this->language,$element);
	}
	
	
	/**
	 * -Sprachen abfragen-
	 * Gibt ein Array mit allen verfügbaren Sprachen zurück.
	 * @return Array Numerisches Array mit Sprachen.
	 * @since 0.1
	 */
	public static function getLanguages() {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `language` FROM `language` GROUP BY `language`");
		$data = $DB->execute();

		//Sprachen in ein numerisches Array einsortieren
		$return = Array();
		foreach ($data as $value)
		{
			array_push($return, $value['language']);
		}
		return $return;
	}
}
?>