<?php
namespace OpenFTP\Classes;
/**
 * Klasse zur Gruppenverwaltung
 * 
 * @author Sascha Sauermann
 * @since 0.1     
 */
class Group {
	/**
	 * GruppenID
	 * @var integer
	 */
	private $groupID;
	/**
	 * Datenbankobjekt
	 * @var Database
	 */
	private $Database;
	
	/**
	 * -Konstruktor-
	 * 
	 * Erstellt eine neue Datenbankverbindung,legt $groupID auf die ID der aktuell verwalteten Gruppe fest
	 * [und erstellt gegebenenfalls eine neue Gruppe mit dem übergebenen Namen]
	 * 
	 * @param String $groupName Name der Gruppe, die verwaltet werden soll.
	 * @param boolean $create [optional=FALSE] Wenn TRUE wird versucht eine neue Gruppe mit dem Namen $groupName anzulegen.
	 * @since 0.1
	 * @throws Exceptions\GroupAlreadyExistsException
	 * @throws Exceptions\GroupNotFoundException
	 */
	public function __construct($groupName, $create = FALSE) 
	{
		//Datenbankverbindung herstellen
		$this->Database = $GLOBALS['Database'];
		//Datenbank auf Vorkommen mit Gruppennamen überprüfen
		$this->Database->prepareStatement("SELECT COUNT(*) FROM groups WHERE groupname = :group");
		$data = $this->Database->execute(Array("group"=>$groupName));
		/*
		 * Überprüfen, auf:
		 * 		Gruppe nicht vorhanden && Gruppe erstellen
		 * 		Gruppe vorhanden && Gruppe nicht erstellen
		 * 		Gruppe vorhanden && erstellen
		 * 		Gruppe nicht vorhanden && nicht erstellen [else]
		 */
		if (($data[0]['COUNT(*)']=="0" && $create == TRUE)) {
			//Erstelle neue Gruppe
			$this->Database->prepareStatement("INSERT INTO groups (`groupname`) VALUES (:group)");
			$data = $this->Database->execute(Array("group"=>$groupName));
			//Neue ID $groupID zuweisen
			$this->groupID = $this->Database->lastInsertID("group_ID");
		}
		elseif ($data[0]['COUNT(*)']=="1" && $create == FALSE)
		{
			//Frage ID der Gruppe mit $groupName ab
			//Neue ID $groupID zuweisen
			$this->groupID = Group::getGroupID($groupName);
		}
		elseif ($create==TRUE)
		{		
			//Gruppe anlegen: Gruppe mit gleichem Namen bereits vorhanden
			throw new Exceptions\GroupAlreadyExistsException(Group::getGroupID($groupName));
		}
		else	
		{
			//-Gruppe "benutzen": Gruppe nicht vorhanden
			throw new Exceptions\GroupNotFoundException(NULL,$groupName);

		}
	}
	
	/**
	 * -Destruktor-
	 * Gibt alle Attribute wieder frei
	 * @since 0.1
	 */
	public function __destruct() {
		$this->groupID = NULL;
		$this->Database = NULL;
	}
	
	/**
	 * -Gruppe löschen-
	 * Entfernt die verwaltete Gruppe aus der Datenbank.
	 * @since 0.1
	 */
	public function remove() {
		//Gruppe aus jeder Tabelle löschen, in der sie verwendet wird.
		$this->Database->prepareStatement("DELETE `groups`, `userGroups`, `defaultPermissionsGroup`, `filePermissionsGroup`, `folderPermissionsGroup`
				 FROM `groups` LEFT JOIN `userGroups` ON (`groups.group_ID`=`userGroups.group`) LEFT JOIN `defaultPermissionsGroup` ON (`groups.group_ID`=`defaultPermissionsGroup.group`)
				 LEFT JOIN `filePermissionsGroup` ON (`groups.group_ID`=`filePermissionsGroup.group`) LEFT JOIN `folderPermissionsGroup` ON (`groups.group_ID`=`folderPermissionsGroup.group`)
				 WHERE `groups.group_ID` = :id");
		$this->Database->execute(Array("id"=>$this->groupID));
	}
	
	/**
	 * -Gruppenname festlegen-
	 * Setzt den Namen der verwalteten Gruppe auf $name fest.
	 * @param String $name Festzulegender Name
	 * @since 0.1
	 */
	public function setName($name) {
		$this->Database->prepareStatement("UPDATE `groups` SET `groupname`=:name WHERE `group_ID` =:id");
		$this->Database->execute(Array("id"=>$this->groupID,"name"=>$name));
	}
	/**
	 * -Mitglieder auflisten-
	 * Gibt ein Array mit den Mitgliedern der aktuell verwalteten Gruppe zurück.
	 * @return Array Numerisches Array mit den Mitgliedern
	 * @since 0.1
	 */
	public function getMembers() {
		$this->Database->prepareStatement("SELECT `user` FROM `userGroups` WHERE `group` =:id");
		$data = $this->Database->execute(Array("id"=>$this->groupID));

		//Benutzer in ein numerisches Array einsortieren
		$return = Array();
		foreach ($data as $value)
		{
			array_push($return, $value['user']);
		}
		return $return;
	}
	/**
	 * -Mitglieder setzten-
	 * Löscht alle Mitglieder der verwalteten Gruppe und legt die angegebenen Benutzer als Mitglieder fest.
	 * @param Array $users Numerisches Array mit den ID's der festzulegenden Mitglieder.
	 * @since 0.1
	 */
	public function setMembers($users) {
		//Überprüfen ob Gruppe noch keine Member besitzt
		if (count($this->getMembers())>0)
		{
			//Member löschen wenn vorhanden
			$this->removeMembers(-1);
		}
		$this->addMembers($users);
	}
	/**
	 * -Mitglied hinzufügen-
	 * Fügt der verwalteten Gruppe ein Mitglied hinzu.
	 * @param Integer $user ID des hinzuzufügenden Benutzers
	 * @since 0.1
	 * @throws Exceptions\UserAlreadyInGroupException
	 */
	public function addMember($user) {
		//Überprüfen, ob Benutzer existiert (Wenn nicht -> Fehler)
		User::getName($user);
		//Überprüfen, ob Benutzer nicht in der Gruppe (Wenn nicht -> Fehler)
		if (in_array($user, $this->getMembers()))
			throw new Exceptions\UserAlreadyInGroupException($user, $this->groupID);
			
		$this->Database->prepareStatement("INSERT INTO `userGroups` (`group`, `user`) VALUES(:id,:user)");
		$this->Database->execute(Array("id"=>$this->groupID,"user"=>$user));
	}
	/**
	 * -Mitglied entfernen-
	 * Entfernt ein Mitglied aus der verwalteten Gruppe.
	 * @param Integer $user ID des zu entfernenden Benutzers
	 * @since 0.1
	 */
	public function removeMember($user) {
		$this->Database->prepareStatement("DELETE FROM `userGroups` WHERE `user`=:user AND `group`=:id");
		$this->Database->execute(Array("id"=>$this->groupID,"user"=>$user));
	}
	/**
	 * -Mitglieder hinzufügen-
	 * Fügt der verwalteten Gruppe Mitglieder hinzu.
	 * @param Array $users Array mit ID's der hinzuzufügenden Benutzer
	 * @since 0.1
	 */
	public function addMembers($users) {
		foreach ($users as $value)
		{
			$state = $this->addMember($value);
		}	
	}
	/**
	 * -Mitglieder entfernen-
	 * Entfernt Mitglieder aus der verwalteten Gruppe.
	 * @param Array|Integer $users Array mit ID's der zu entfernenden Benutzer. Oder -1 um alle Mitglieder zu entfernen.
	 * @since 0.1
	 */
	public function removeMembers($users) {
		//Alle Benutzer aus Gruppe entfernen
		if ($users<0) {
			$this->Database->prepareStatement("DELETE FROM `userGroups` WHERE `group`=:id");
			$this->Database->execute(Array("id"=>$this->groupID));
		}
		//Bestimmte Benutzer entfernen
		else
		{
			//Jeden übergebenen Benutzer löschen.
			foreach ($users as $value)
			{
				$this->removeMember($value);
			}
		}
	
	}
	/**
	 * -Name zu ID-
	 * Fragt den Namen zu einer ID ab.
	 * @param integer $groupID ID der Gruppe
	 * @return String Name der Gruppe
	 * @since 0.1
	 * @throws Exceptions\GroupNotFoundException
	 */
	public static function getName($groupID) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `groupname` FROM `groups` WHERE `group_ID`=:id");
		$data = $DB->execute(Array("id"=>$groupID));
		//Rückgabe des Namens
		if (count($data)>0)
			return $data[0]['groupname'];
		else
			throw new Exceptions\GroupNotFoundException($groupID);
	}

	/**
	 * -Vorhandene Gruppen-
	 * Fragt die vorhandenen Gruppen ab.
	 * @return Array Numerisches Array mit den ID's der vorhandenen Gruppen.
	 * @since 0.1
	 */
	public static function getGroups() {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `group_ID` FROM `groups`");
		$data = $DB->execute();
		//Rückgabe des Arrays mit den Gruppen
		//Rückgabe der Abfrage umformen
		$return = Array();
		foreach ($data as $value)
		{
			array_push($return, $value);
		}
		return $return;
	}
	/**
	 * -ID zu Name-
	 * @param String $groupName Name der Gruppe
	 * @return Integer ID der Gruppe
	 * @since 0.1
	 * @throws Exceptions\GroupNotFoundException
	 */
	public static function getGroupID($groupName) {
		//Globales Datenbankobjekt verwenden
		$DB = $GLOBALS['Database'];
		$DB->prepareStatement("SELECT `group_ID` FROM `groups` WHERE `groupname`=:name");
		$data = $DB->execute(Array("name"=>$groupName));
		//Rückgabe der ID oder FALSCH
		if (count($data)>0)
			return $data[0]['group_ID'];
		else
			throw new Exceptions\GroupNotFoundException(NULL,$groupName);
	}

}
?>