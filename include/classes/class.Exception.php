<?php
namespace OpenFTP\Classes\Exceptions;

/**
 * -Database Exception-
 * Code: [1]
 * @author Sascha Sauermann
 * @since 0.1
 *        
 */
class DatabaseException extends \Exception 
{
	/**
	 * -Konstruktor-
	 * @param String $message [optional] Fehlernachricht
	 * @param integer $code [optional] Fehlercode Standard: 1
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct($message=NULL, $code=NULL, \PDOException $previous=NULL)
	{
		if ($code===NULL)
			$code=1;
			parent::__construct($message, $code, $previous);
	}
}

/**
 * -DB Connection Exception-
 * Code: 4
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DBConnectionException extends DatabaseException 
{
	protected $dsn;
	
	/**
	 * -Konstruktor-
	 * @param String $dsn [optional] Datenbankverbindungszeichenfolge
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct($dsn=NULL, \PDOException $previous=NULL)
	{
		$this->dsn = $dsn;
		parent::__construct("Can't connect to database with DSN '$dsn'", 4, $previous);
	}

	/**
	 * @return String Datenbankverbindungszeichenfolge
	 */
	public final function getDSN() {
		return $this->dsn;
	}
}

/**
 * -DB StatementPreparation Exception-
 * Code: 5
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DBStatementPreparationException extends DatabaseException 
{
	protected $statement;
	
	/**
	 * -Konstruktor-
	 * @param String $statement [optional] Prepared Statement
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct($statement=NULL, \PDOException $previous=NULL)
	{
		$this->statement = $statement;
		parent::__construct("Can't prepare statement '$statement'", 5, $previous);
	}

	/**
	 * @return String Prepared Statement
	 */
	public final function getStatement() {
		return $this->statement;
	}
}

/**
 * -DB NoStatement Exception-
 * Code: 6
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DBNoStatementException extends DatabaseException 
{

	/**
	 * -Konstruktor-
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct(\PDOException $previous=NULL)
	{
		parent::__construct("No prepared statement", 6, $previous);
	}
}

/**
 * -DB Query Exception-
 * Code: 7
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DBQueryException extends DatabaseException 
{
	protected $query;
	protected $args;
	
	/**
	 * -Konstruktor-
	 * @param String $query [optional] Datenbankabfrage
	 * @param Array $args [optional] Argumente
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct($query=NULL, $args=NULL, \PDOException $previous=NULL)
	{
		$this->query = $query;
		$this->args = $args;
		parent::__construct("Can't execute query '$query'", 7, $previous);
	}

	/**
	 * @return String Datenbankabfrage
	 */
	public final function getQuery() {
		return $this->query;
	}
	
	/**
	 * @return Array Argumente
	 */
	public final function getArguments() {
		return $this->args;
	}
}

final class DBExecuteStatementException extends DatabaseException 
{
	protected $errorInfo;
	protected $args;
	
	/**
	 * -Konstruktor-
	 * @param Array $args [optional] Argumente
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct($args=NULL, \PDOException $previous=NULL)
	{
		$this->args = $args;
		parent::__construct("Can not execute prepared statement", 3, $previous);
	}

	/**
	 * @return Array Argumente
	 */
	public final function getArguments() {
		return $this->args;
	}
}


/**
 * -DB PDOFunction Exception-
 * Code: 8
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DBPDOFunctionException extends DatabaseException 
{
	/**
	 * -Konstruktor-
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct(\PDOException $previous=NULL)
	{
		parent::__construct("Can't use PDO function", 8, $previous);
	}
}

/**
 * -DB NoRowsInserted Exception-
 * Code: 9
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DBNoRowsInsertedException extends DatabaseException 
{
	protected $query;
	
	/**
	 * -Konstruktor-
	 * @param String $query [optional] Datenbankabfrage
	 * @param \PDOException $previous [optional] Vorheriger Fehler
	 */
	public function __construct($query=NULL, \PDOException $previous=NULL)
	{
		$this->query = $query;
		parent::__construct("No inserted rows by query '$query'", 9, $previous);
	}

	/**
	 * @return String Datenbankabfrage
	 */
	public final function getQuery() {
		return $this->query;
	}
	
}

/**
 * -NotFound Exception-
 * Code: 10
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
class NotFoundException extends \Exception
{
	/**
	 * -Konstruktor-
	 * @param String $message [optional] Fehlernachricht
	 * @param integer $code [optional] Fehlercode Standard: 10
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($message=NULL, $code=NULL, Exception $previous=NULL)
	{
		if ($code===NULL)
			$code=10;
			parent::__construct($message, $code, $previous);
	}
}

/**
 * -UserNotFound Exception-
 * Code: 11
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class UserNotFoundException extends NotFoundException {
	protected $user;
	protected $userID;
	
	/**
	 * -Konstruktor-
	 * @param String $user [optional] Benutzer
	 * @param integer $userID [optional] BenutzerID
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($user=NULL, $userID=NULL, Exception $previous=NULL)
	{
		$this->user = $user;
		$this->userID=$userID;
		if ($user!==NULL)
			parent::__construct("No entry for user '$user'", 11, $previous);
		else
			parent::__construct("No entry for user with ID '$userID'", 11, $previous);
	}

	/**
	 * @return String Benutzer
	 */
	public final function getUser() {
		return $this->user;
	}
	
	/**
	 * @return String BenutzerID
	 */
	public final function getUserID() {
		return $this->userID;
	}
	
}

/**
 * -GroupNotFound Exception-
 * Code: 12
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class GroupNotFoundException extends NotFoundException {
	protected $groupID;
	protected $group;
	
	/**
	 * -Konstruktor-
	 * @param integer $groupID [optional] GruppenID
	 * @param String $group [optional] Gruppenname
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($groupID=NULL, $group=NULL, Exception $previous=NULL)
	{
		$this->group = $group;
		$this->groupID = $groupID;
		
		if ($group!==NULL)
			parent::__construct("No entry for group '$group'", 12, $previous);
		else
			parent::__construct("No entry for group with ID '$groupID'", 12, $previous);
	}

	/**
	 * @return integer GruppenID
	 */
	public final function getGroupID() {
		return (integer) $this->groupID;
	}
	
	/**
	 * @return  String Gruppenname
	 */
	public final function getGroupname() {
		return (integer) $this->groupname;
	}
	
}

/**
 * -FileEntryNotFound Exception-
 * Code: 13
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class FileEntryNotFoundException extends NotFoundException {
	protected $fileID;
	protected $dir;
	protected $filename;
	
	/**
	 * -Konstruktor-
	 * @param integer $fileID [optional] DateiID
	 * @param String $dir [optional] Ordner
	 * @param String $filename [optional] Dateiname
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($fileID=NULL, $dir=NULL, $filename=NULL, Exception $previous=NULL)
	{
		$this->fileID = $fileID;
		$this->dir = $dir;
		$this->filename = $filename;
		if ($filename!==NULL)
			parent::__construct("No entry for file '".$dir.$filename."'", 13, $previous);
		else
			parent::__construct("No entry for file with ID '$fileID'", 13, $previous);
	}

	/**
	 * @return integer DateiID
	 */
	public final function getFileID() {
		return (integer) $this->fileID;
	}
	
	/**
	 * @return String Dateiname
	 */
	public final function getFilename() {
		return $this->filename;
	}
	
	/**
	 * @return String Ordner
	 */
	public final function getDirectory() {
		return $this->dir;
	}
	
}

/**
 * -DirectoryEntryNotFound Exception-
 * Code: 14
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class DirectoryEntryNotFoundException extends NotFoundException {
	protected $dirID;
	protected $path;
	
	/**
	 * -Konstruktor-
	 * @param String $dirID [optional] OrdnerID
	 * @param String $path [optional] Ordnerpfad
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($dirID=NULL, $path=NULL, Exception $previous=NULL)
	{
		$this->dirID = $dirID;
		$this->path = $path;
		if ($path!=NULL)
			parent::__construct("No entry for directory with ID '$dirID'", 14, $previous);
		else
			parent::__construct("No entry for directory '$path'", 14, $previous);
	}

	/**
	 * @return integer OrdnerID
	 */
	public final function getDirectoryID() {
		return (integer) $this->dirID;
	}
	
	/**
	 * @return String Ordnerpfad
	 */
	public final function getDirectoryPath() {
		return $this->path;
	}
	
}

/**
 * -ModuleNotFound Exception-
 * Code: 15
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class ModuleNotFoundException extends NotFoundException {
	protected $moduleID;
	protected $module;
	
	/**
	 * -Konstruktor-
	 * @param integer $moduleID [optional] ModuleID
	 * @param String $module [optional] ModuleName
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($moduleID=NULL, $module=NULL, Exception $previous=NULL)
	{
		$this->moduleID = $moduleID;
		$this->module = $module;
		if ($module!=NULL)
			parent::__construct("No entry for module '$module'", 15, $previous);
		else
			parent::__construct("No entry for module with ID '$moduleID'", 15, $previous);
	}

	/**
	 * @return integer ModuleID
	 */
	public final function getModuleID() {
		return (integer) $this->moduleID;
	}
	
	/**
	 * @return String Module
	 */
	public final function getModule() {
		return $this->module;
	}
	
}

/**
 * -SettingNotFound Exception-
 * Code: 16
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class SettingNotFoundException extends NotFoundException {
	protected $setting;
	
	/**
	 * -Konstruktor-
	 * @param integer $setting [optional] Einstellung
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($setting=NULL, Exception $previous=NULL)
	{
		$this->setting = $setting;
		parent::__construct("No entry for setting '$setting'", 16, $previous);
	}

	/**
	 * @return integer Setting
	 */
	public final function getSetting() {
		return (integer) $this->setting;
	}
	
}

/**
 * -LanguageNotFound Exception-
 * Code: 17
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class LanguageNotFoundException extends NotFoundException {
	protected $lang;
	
	/**
	 * -Konstruktor-
	 * @param String $language [optional] Sprache
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($language=NULL, Exception $previous=NULL)
	{
		$this->lang = $language;
		parent::__construct("No entry for language '$language'", 17, $previous);
	}

	/**
	 * @return String Sprache
	 */
	public final function getLanguage() {
		return $this->lang;
	}
	
}

/**
 * -LanguageElementNotFound Exception-
 * Code: 18
 * @author Sascha Sauermann
 * @since 0.1
 *       
 */
final class LanguageElementNotFoundException extends NotFoundException {
	protected $lang;
	protected $element;
	
	/**
	 * -Konstruktor-
	 * @param String $language [optional] Sprache
	 * @param String $element [optional] Element
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($language=NULL, $element=NULL, Exception $previous=NULL)
	{
		$this->lang = $language;
		$this->element = $element;
		parent::__construct("No entry for element '$element' and language '$language'", 18, $previous);
	}

	/**
	 * @return String Sprache
	 */
	public final function getLanguage() {
		return $this->lang;
	}
	
	/**
	 * @return String Element
	 */
	public final function getElement() {
		return $this->element;
	}
	
}

/**
 * -AlreadyExists Exception-
 * Code: [20]
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
class AlreadyExistsException extends \Exception 
{
	/**
	 * -Konstruktor-
	 * @param String $message [optional] Fehlernachricht
	 * @param integer $code [optional] Fehlercode Standard: 20
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($message=NULL, $code=NULL, Exception $previous=NULL)
	{
		if ($code===NULL)
			$code=20;
			parent::__construct($message, $code, $previous);
	}
}

/**
 * -UserAlreadyExists Exception-
 * Code: 21
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class UserAlreadyExistsException extends AlreadyExistsException
{
	protected $user;
	
	/**
	 * -Konstruktor-
	 * @param String $user [optional] Benutzer
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($user=NULL, Exception $previous=NULL)
	{
		$this->user = $user;
		parent::__construct("User '$user' already existing", 21, $previous);
	}

	/**
	 * @return integer Benutzer
	 */
	public final function getUser() {
		return $this->user;
	}
}

/**
 * -GroupAlreadyExists Exception-
 * Code: 22
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class GroupAlreadyExistsException extends AlreadyExistsException
{
	protected $groupID;
	
	/**
	 * -Konstruktor-
	 * @param integer $groupID [optional] GruppenID
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($groupID=NULL, Exception $previous=NULL)
	{
		$this->groupID = $groupID;
		parent::__construct("Group '$groupID' already existing", 22, $previous);
	}

	/**
	 * @return integer GruppenID
	 */
	public final function getGroupID() {
		return (integer) $this->groupID;
	}
}

/**
 * -UserAlreadyInGroup Exception-
 * Code: 23
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class UserAlreadyInGroupException extends AlreadyExistsException
{
	protected $userID;
	protected $groupID;
	
	/**
	 * -Konstruktor-
	 * @param integer $userID [optional] BenutzerID
	 * @param integer $groupID [optional] GruppenID
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($userID=NULL, $groupID=NULL, Exception $previous=NULL)
	{
		$this->userID = $userID;
		$this->groupID = $groupID;
		parent::__construct("User '$userID' already in group '$groupID'", 23, $previous);
	}

	/**
	 * @return integer BenutzerID
	 */
	public final function getUserID() {
		return (integer) $this->userID;
	}
	
	/**
	 * @return integer GruppenID
	 */
	public final function getGroupID() {
		return (integer) $this->groupID;
	}
}

/**
 * -I/O Exception-
 * Code: [61]
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
class IOException extends \Exception 
{
	/**
	 * -Konstruktor-
	 * @param String $message [optional] Fehlernachricht
	 * @param integer $code [optional] Fehlercode Standard: 61
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($message=NULL, $code=NULL, Exception $previous=NULL)
	{
		if ($code===NULL)
			$code=61;
			parent::__construct($message, $code, $previous);
	}
}

/**
 * -FileNotFound Exception-
 * Code: 62
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class FileNotFoundException extends IOException
{
	protected $file;
	
	/**
	 * -Konstruktor-
	 * @param String $file [optional] Pfad zur Datei
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($file=NULL, Exception $previous=NULL)
	{
		$this->file = $file;
		parent::__construct("File '$file' not found", 62, $previous);
	}

	/**
	 * @return String Pfad der Datei
	 */
	public final function getFilepath() {
		return $this->file;
	}
}

/**
 * -DirectoryNotFound Exception-
 * Code: 63
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DirectoryNotFoundException extends IOException
{
	protected $dir;
	/**
	 * -Konstruktor-
	 * @param String $dir [optional] Pfad zum Ordner
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($dir=NULL, Exception $previous=NULL)
	{
		$this->dir = $dir;
		parent::__construct("Directory '$dir' not found", 63, $previous);
	}

	/**
	 * @return String Pfad des Ordners
	 */
	public final function getDir() {
		return $this->dir;
	}
}

/**
 * -FileAlreadyExists Exception-
 * Code: 64
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class FileAlreadyExistsException extends IOException
{
	protected $file;
	/**
	 * -Konstruktor-
	 * @param String $file [optional] Pfad zur Datei
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($file=NULL, Exception $previous=NULL)
	{
		$this->file = $file;
		parent::__construct("File '$file' already existing", 64, $previous);
	}

	/**
	 * @return String Pfad der Datei
	 */
	public final function getFilepath() {
		return $this->file;
	}
}

/**
 * -DirectoryAlreadyExists Exception-
 * Code: 65
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class DirectoryAlreadyExistsException extends IOException
{
	protected $dir;
	/**
	 * -Konstruktor-
	 * @param String $dir [optional] Pfad zum Ordner
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($dir=NULL, Exception $previous=NULL)
	{
		$this->dir = $dir;
		parent::__construct("Directory '$dir' already existing", 65, $previous);
	}

	/**
	 * @return String Pfad des Ordners
	 */
	public final function getDir() {
		return $this->dir;
	}
}

/**
 * -AccessDenied Exception-
 * Code: 66
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class AccessDeniedException extends IOException {
	/**
	 * Lesender Zugriff verweigert
	 * @var integer
	 * @since 0.1
	 */
	const READ = 1;
	/**
	 * Schreibender Zugriff verweigert
	 * @var integer
	 * @since 0.1
	 */
	const WRITE = 2;
	protected $accessType;
	/**
	 * -Konstruktor-
	 * @param String $file [optional] Datei/Ordner auf den nicht zugegriffen werden kann
	 * @param integer $deniedAccess [optional] Art des Zugriffs, der verweigert wurde
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($file=NULL, $deniedAccess=NULL, Exception $previous=NULL)
	{
		$this->accessType = $deniedAccess;
		parent::__construct("Access to '$file' denied", 66, $previous);
	}

	/**
	 * @return integer Art des Zugriffs, der verweigert wurde.
	 */
	public final function getDeniedAccess() {
		return $this->accessType;
	}
}

/**
 * -NotFile Exception-
 * Code: 67
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class NotFileException extends IOException
{
	protected $file;
	/**
	 * -Konstruktor-
	 * @param String $file [optional] Pfad zur Datei
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($file=NULL, Exception $previous=NULL)
	{
		$this->file = $file;
		parent::__construct("'$file' is not a file", 67, $previous);
	}

	/**
	 * @return String Pfad der Datei
	 */
	public final function getFilepath() {
		return $this->file;
	}
}

/**
 * -NotDirectory Exception-
 * Code: 68
 * @author Sascha Sauermann
 * @since 0.1
 *
 *        
 */
final class NotDirectoryException extends IOException
{
	protected $file;
	/**
	 * -Konstruktor-
	 * @param String $dir [optional] Pfad zum Ordner
	 * @param Exception $previous [optional] Vorheriger Fehler
	 */
	public function __construct($dir=NULL, Exception $previous=NULL)
	{
		$this->dir = $dir;
		parent::__construct("'$dir' is not a directory", 68, $previous);
	}

	/**
	 * @return String Pfad des Ordners
	 */
	public final function getFolder() {
		return $this->dir;
	}
}

/**
 * -LogicException-
 * Code: 91
 * @author Sascha Sauermann
 * @since 0.1
 */
final class LogicException extends \Exception
{
	const LOGIN_FAILED = "LOGIN_FAILED";
	
	protected $exType;
	
	/**
	 * -Konstruktor-
	 * @param String $exceptionType Typ der Ausnahme (Konstante)
	 */
	public function __construct($exceptionType=NULL) {
		$this->exType=$exceptionType;
		parent::__construct("Logic exception of type '".$exType."'", 91, NULL);
	}
	/**
	 * @return String Typ der Ausnahme
	 */
	public final function getExceptionType() {
		return $this->exType;
	}
}
?>