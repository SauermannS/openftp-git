<?php
namespace OpenFTP\Classes;

class ExceptionHandler {
	
	public function __construct()
	{
		set_exception_handler(Array($this, 'handleException'));
	}
	public function handleException($exception)
	{
		echo "<b>Exception:</b>";
		echo "<br/><br/>";
	
		$counter = 1;
		$exit = FALSE;
		$prevEx = $exception;
	
		while (!$exit)
		{
			echo "<b>Stack " . $counter . "</b>:<br/>";
			echo "Msg: " . $prevEx->getMessage() . "<br/>";
			echo "Code: " . $prevEx->getCode() . "<br/>";
			echo "File: " . $prevEx->getFile() . "<br/>";
			echo "Line: " . $prevEx->getLine() . "<br/>";
			echo "Trace: " . $prevEx->getTraceAsString() . "<br/><br/>";
	
			$prevEx = $prevEx->getPrevious();
			$counter++;
			if ($prevEx==NULL || $counter>10)
				$exit = TRUE;
		}

	}


}