<?php
namespace OpenFTP\Classes;
class ErrorHandler{
	
	public function __construct() {
		set_error_handler(Array($this, 'handleError'));
	}
	public function handleError($number, $string, $file, $line, $context) {

		//Fehler wurde via @ unterdrückt
		if (0 === error_reporting()) {
			return TRUE;
		}
		// Determine if this error is one of the enabled ones in php config (php.ini, .htaccess, etc)
  		  $error_is_enabled = (bool)($number & ini_get('error_reporting') );
   
  		  // -- FATAL ERROR
  		  // throw an Error Exception, to be handled by whatever Exception handling logic is available in this context
  		  if( in_array($number, array(E_USER_ERROR, E_RECOVERABLE_ERROR)) && $error_is_enabled ) {
 		       throw new \ErrorException($string, $number, 0, $file, $line);
 		   }
   
 		   // -- NON-FATAL ERROR/WARNING/NOTICE
 		   // Log the error if it's enabled, otherwise just ignore it
  		  else if( $error_is_enabled ) {
  		      error_log( $string, 0 );
   		     return TRUE; // Make sure this ends up in $php_errormsg (false), if appropriate
   		 }
	}
}

?>