<?php
namespace OpenFTP\Module;
abstract class Module {
	public final function show($params=Array())
	{
		try
		{
			echo static::getContent($params);
		}
		catch (\Exception $e)
		{
			//TODO: Fehlerhandling über Exception Klasse
			echo "<p>".$e->getMessage()."</p>";
			die();
		}
	}
	public final function process($params=Array())
	{
		try 
		{
			echo static::processContent($params);
		}
		catch (\Exception $e)
		{
			//TODO: Fehlerhandling über Exception Klasse
			echo "<p>".$e->getMessage()."</p>";
			die();
		}
	}
	protected abstract function getContent($params);
	protected abstract function processContent($params);
}
?>