<?php
namespace OpenFTP\Module;


class Demomodule2 extends Module 
{
	protected function getContent($params) {
		$return = "";
		$return .= '<script type="text/javascript">
					function processDemomodule2Login()
					{
						var m = new Module("Demomodule2");
						var params = $("#demo2form").serialize();
						params += "&mode=login";
						m.processModule(params);
					}
					</script>';
		$return .= '<script type="text/javascript">
					function processDemomodule2Logout()
					{
						var m = new Module("Demomodule2");
						var params = "mode=logout";
						m.processModule(params);
					}
					</script>';
		$return .= "<h3>Demologin</h3><br/>";
		
			$userID = \OpenFTP\Classes\User::getSessionID();
		if ($userID==0)
		{	
			$return .= '<form id="demo2form">
 					Username: <input type="text" name="username" size="15" /><br />
  					Password: <input type="password" name="password" size="15" /><br />
  					<div>
    				<p><input type="button" value="Login" onClick=\'processDemomodule2Login();\'/></p>
 					</div>
					</form>';
		}
		else 
		{
			$return .= "Sie sind bereits als Benutzer '".\OpenFTP\Classes\User::getName($userID)."' eingeloggt.<br/>";
			$return .= '<input type="button" value="Logout" onClick=\'processDemomodule2Logout();\'/>';
		}
		
		return $return;
	}
	
	protected function processContent($params)
	{
		echo "Folgende Daten wurden an den Server übertragen:<br/><br/>";
		var_dump($params);
		echo "<br/><br/>";
		
		switch ($params['mode'])
		{
			case "login":
			{
				$user = new \OpenFTP\Classes\User($params['username']);
				$user->login($params['password']);
				echo "Erfolgreich angemeldet.";
				break;
			}
			case "logout":
			{
				session_destroy();
				echo "Sie wurden abgemeldet.";
				break;
			}
		}
	}
	
}

?>