<?php
namespace OpenFTP;

require_once (__DIR__ . DIRECTORY_SEPARATOR . "settings.php");

require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Database.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Settings.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Group.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.User.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Exception.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.File.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Language.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Module.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.Permissions.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.ErrorHandler.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.SessionHandler.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "class.ExceptionHandler.php");


require_once (__DIR__ . DIRECTORY_SEPARATOR . "module" . DIRECTORY_SEPARATOR . "module.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "module" . DIRECTORY_SEPARATOR . "module.demo.php");
require_once (__DIR__ . DIRECTORY_SEPARATOR . "module" . DIRECTORY_SEPARATOR . "module.demo2.php");

$ErrorHandler = new Classes\ErrorHandler();
$ExceptionHandler = new Classes\ExceptionHandler();
$Database = new Classes\Database ('mysql:host='.DB_SERVER.';port='.DB_PORT.';dbname='.DB_DB, DB_USER, DB_PASSWORD);
$Sessionhandler = new Classes\SessionHandler();

session_start();

require_once (__DIR__ . DIRECTORY_SEPARATOR . "packets" . DIRECTORY_SEPARATOR . "packet.ScreenOut.php");;


?>